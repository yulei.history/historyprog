# 说明 

这个程序可以把写作中的历史书转换为HTML和PDF文件

这本历史书的文件见 https://gitlab.com/yulei.history/civ 或者 https://github.com/yulei88/civ


# 使用环境
只能在Linux下运行，（我在Manjaro Linux上工作，其他的发行版没有试过）

本程序使用python3

需要安装：

  * python的 wand 和 pypinyin, pypdf 模块
  * openssl-1.1 库
  * node.js 以及node.js的 mathjax-node-sre 库 （用于处理嵌入的数学公式）
  * 字体： "AR PL UKai CN",  "AR PL UMing CN"（ttf-arphic-uming ttf-arphic-ukai）对应Windows下的 "KaiTi", "SimSun",
  * inkscape（外部程序，wand库用以转换SVG文件）

# 使用方法

先从上述GIT库中取得最新的历史书的work文件（work文件格式的说明见历史书中的README文件）,这些文件放置于某个目录下。

`python3 civmakehtml2.py --input_dir=历史书所在目录 --output_dir=输出文件目录`

生成全部书籍PDF和HTML文件

`python3 civmakehtml2.py 20 --input_dir=历史书所在目录 --output_dir=输出文件目录`

仅生成20部分的文件

在生成的目录下，会看到生成的PDF文件及HTML文件。对于每个部分，比如10，生成10.pdf。HTML文件则在10/html目录下。
