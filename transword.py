#!/usr/bin/env python
# -*- coding: utf-8 -*-


import getopt
import logging
import sys

from modules.config import logger
from modules.translate import Translation


def Usage():
    print("Usage: pythone3 transword.py [-h] [--help] [-o] [汉语]")
    print("    根据缓存的译名表，取到汉语对应的英文")
    print("    若没有提供汉语参数，则缺省使用标准输入")
    print("    -o 如果设置，返回缓存中相应的其他内容")
    print("    -h或--help 显示本说明内容")
    print("  举例： pythone3 transword.py 汉语")
    print("       如果缓存表中不存在‘汉语’，返回显示  `汉语[]")
    print("        pythone3 transword.py 孔子")
    print("       如果缓存表中存在‘孔子’，返回显示  `孔子[Confucius]")
    print("        pythone3 transword.py -o 孔子")
    print("       返回显示译名及附加内容  `孔子[Confucius;前551/9/28-前479/4/11]")
    print("       如果参数是多行的，则每一行作为一个汉语，返回多行，而且会删除同一汉语的重复")
    exit()


def getChinese(entry):
    line = entry.strip()
    status = 0  # 0 普通文字， 1 进入`中文状态，2 进入[英文状态
    chinese = ""
    for char in line:
        if status == 0:
            if char == '`':
                status = 1
            else:
                chinese += char
            continue
        if status == 1:
            if char == '[':
                status = 2
            else:
                chinese += char
            continue
        if status == 2:
            continue
    return chinese


# 主程序这里开始
try:
    options, args = getopt.gnu_getopt(sys.argv[1:], "ohd",
                                      ['help', 'debug'])
except getopt.GetoptError:
    Usage()

OTHER = False
for o, a in options:
    if o in ("-o"):
        OTHER = True
    elif o in ("-h", "--help"):
        Usage()
    elif o in ("-d", "--debug"):
        logger.setLevel(logging.DEBUG)

Translation.readCache("./output/cache")

if len(args) > 0:
    entry = " ".join(args)
    logger.debug(entry)
    words = entry.split("\n")
else:
    words = sys.stdin.readlines()

clist = {}
for c in words:
    chinese = getChinese(c)
    logger.debug(chinese)
    if len(chinese) == 0:
        continue
    if chinese in clist.keys():
        continue
    rtn = '`%s[' % chinese
    if chinese in Translation.trWord.keys():
        rtn += Translation.trWord[chinese].english
        if OTHER:
            rtn += ';' + Translation.trWord[chinese].other
    rtn += ']'
    clist[chinese] = rtn

keys = list(clist.keys())
keys.sort()
output = []
for c in keys:
    output.append(clist[c])

print("\n".join(output), end="")
