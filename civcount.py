#!/usr/bin/env python
# -*- coding: utf-8 -*-
import getopt
import logging
import os
import sys
from modules.config import logger

# 这个程序对写作的work进行字数统计

def Usage():
    print("Usage: pythone3 civcount.py input_dir ")
    exit()


# 主程序这里开始

WORKFILEPATH = './civ/'

try:
    options, args = getopt.getopt(sys.argv[1:], "tdDqo",
                                  ["input_dir="])
except getopt.GetoptError:
    Usage()

verbosedebug = False
quietmode = False
includeothers = False

for o, a in options:
    if o in ("--input_dir"):
        WORKFILEPATH = a
    elif o == "-o":
        includeothers = True
    elif o == "-d":
        testenv = "/debug"
        logger.setLevel(logging.DEBUG)
    elif o == "-q":
        quietmode = True
    elif o == "-D":
        testenv = "/debug"
        logger.setLevel(logging.DEBUG)

booklist = []

files = os.listdir(WORKFILEPATH)
for file in files:
    file_path = os.path.join(WORKFILEPATH, file)
    if file[0] != '.' and file[0] != '_' and os.path.isdir(file_path):
        booklist.append(file)

booklist.sort()

if len(args) > 0:
    booklist1 = []
    for book in args:
        if not book in booklist:
            dir_path = os.path.join(WORKFILEPATH, book)
            if not os.path.isdir(dir_path):
                logger.error("没有%s，全部的列表是%s" % (book, ",".join(booklist)))
                quit()
        booklist1.append(book)
    booklist = booklist1

CountAllText = 0  # 全部文字计数
for book in booklist:
    inputdirname = os.path.join(WORKFILEPATH, book)
    CountDirText = 0  # 一个目录下的文字计数

    inputdirfiles = os.listdir(inputdirname)
    workfiles = []
    for f in inputdirfiles:
        if f.endswith(".work"):
            if os.path.isfile(inputdirname + '/' + f):
                workfiles.append(f)
        if includeothers and f == 'others':
            othersdir = inputdirname + '/' + f
            if os.path.isdir(othersdir):
                otherfiles = os.listdir(othersdir)
                for f1 in otherfiles:
                    if f1.endswith(".work"):
                        if os.path.isfile(othersdir + '/' + f1):
                            workfiles.append(f+"/"+f1)


    if len(workfiles) == 0:
        logger.warning('指定目录下没有work文件')

    workfiles.sort()

    for workfile in workfiles:
        if workfile == 'tr.work':
            continue
        CountText = 0
        inputfilename = inputdirname + "/" + workfile
        inputfile = open(inputfilename)

        # Contents.append("<p>")
        # 初始时状态
        inBrace = False
        line_no = 0  # 行号

        for inputline in inputfile:
            line_no += 1
            line = inputline.strip()

            for char in line:
                if inBrace:
                    if char == '}':
                        inBrace = False
                    continue
                elif char == '{':
                    if not inBrace:
                        inBrace = True
                    continue
                elif char == '`' or char == '[' or char == ']' or char == '{' or char == '}':
                    continue
                CountText += 1

        if not quietmode:
            print("\t%s/%s: %d" % (book, workfile, CountText))
        CountDirText += CountText
        CountAllText += CountText
        if len(inputfilename) != 0:
            inputfile.close()
    if not quietmode:
        print("%s: %d" % (inputdirname, CountDirText))

if not quietmode:
    print("合计: %d" % CountAllText)
else:
    print("%d" % CountAllText)
