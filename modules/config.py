# 各种设置
import sys
import logging
from logging import config

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(asctime)s [%(levelname)s] %(filename)s:%(lineno)d(%(funcName)s) %(message)s'
        },
        'normal': {
            # 'format': '%(asctime)s %(thread)d [%(levelname)s] %(filename)s:%(lineno)d(%(funcName)s) %(message)s'
            'format': '%(asctime)s [%(levelname)s] %(message)s'
        },
    },
    # 'handlers': {
    #     'stderr': {
    #         'class': 'logging.StreamHandler',
    #         'stream': sys.stderr,
    #         'formatter': 'normal',
    #     },
    # },
    'loggers': {
        'my-logger': {
            'handlers': [],
            'level': logging.DEBUG,
            'propagate': True,
        },
    }
}

config.dictConfig(LOGGING)
logger = logging.getLogger("my-logger")
