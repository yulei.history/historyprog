# -*- coding: utf-8 -*-
import json
import os

import pypinyin
from functools import cmp_to_key

from modules.config import logger
from modules.fileinfo import ReadInfo


def compinyin(x, y):
    xpy = pypinyin.pinyin(x, style=pypinyin.NORMAL)
    ypy = pypinyin.pinyin(y, style=pypinyin.NORMAL)
    x1 = xpy[0][0]
    y1 = ypy[0][0]
    return 1 if x1 > y1 else -1 if x1 < y1 else 0


class Translation:
    __line = []  # 见ProcessWord函数开头的说明
    trWord = {}  # 汉语和英语对照表，key是汉语，value是Translation对象本身

    @staticmethod
    def reset():
        Translation.__line = []
        Translation.trWord = {}

    def __init__(self, chinese, chinese2, english, other, file=None):
        self.chinese = chinese
        self.chinese2 = chinese2
        self.english = english
        self.other = other  # 分号以后的内容
        self.ifCache = True
        if file is not None:
            self.dirname = file.dirname
            self.filename = file.filename
            self.lineno = file.lineno
            self.bookid = file.bookid

    @staticmethod
    def writeCache(outputdir):
        filename = outputdir + '/translate_data.json'
        data = {}

        for key in Translation.trWord.keys():
            tr = Translation.trWord[key]
            d = {'chinese': tr.chinese, 'chinese2': tr.chinese2, 'english': tr.english,
                 'other': tr.other, 'filename': tr.filename,
                 'dirname': tr.dirname, 'lineno': tr.lineno, 'bookid': tr.bookid}
            data[key] = d

        with open(filename, "w") as f:
            f.write(json.dumps(data, sort_keys=True, indent=4))
        f.close()

    @staticmethod
    def readCache(outputdir):
        filename = outputdir + '/translate_data.json'
        Translation.__line = []
        Translation.trWord = {}
        if os.path.exists(filename):
            with open(filename, "r") as f:
                jsondata = f.read()
            data = json.loads(jsondata)
            f.close()
            for c in data.keys():
                tr = Translation(data[c]['chinese'], data[c]['chinese2'],
                                 data[c]['english'], data[c]['other'])
                tr.dirname = data[c]['dirname']
                tr.filename = data[c]['filename']
                tr.lineno = data[c]['lineno']
                tr.bookid = data[c]['bookid']
                Translation.trWord[c] = tr

    @staticmethod
    def firstPinyin(string):
        if string is None:
            return None
        lst = list(string)
        c = lst[0:1]
        py = pypinyin.pinyin(c, style=pypinyin.NORMAL)
        return py[0][0][0].upper()

    @staticmethod
    def ProcessWord(line, file: ReadInfo):

        # 在处理译名时，如果一句话里就有一个`，那就会调用ProcessWord处理整行，
        # 所以这里必须判断一下，如果处理过这行就不必再处理一遍
        if line in Translation.__line:
            return
        else:
            Translation.__line.append(line)

        status = 0  # 0 普通文字， 1 进入`中文状态，2 进入[英文状态, 3进入)之后，4进入;之后
        chinese = ""
        chinese2 = ""
        english = ""
        other = ""
        part1 = ""
        for char in line:
            if status == 0:
                if char == '`':
                    status = 1
                continue
            if status == 1:
                if char == '[':
                    status = 2
                else:
                    chinese += char
                continue
            if status == 2:
                if char == ';':
                    english = part1
                    status = 4
                    continue
                elif char == '|' or char == ')' or char == '）':
                    chinese2 = part1
                    status = 3
                    continue
                elif char == ']':
                    english = part1
                    Translation.addWord(chinese, "", english, "", file)
                    chinese = ""
                    chinese2 = ""
                    english = ""
                    other = ""
                    part1 = ""
                    status = 0
                else:
                    part1 += char
            if status == 3:
                if char == ';':
                    status = 4
                    continue
                elif char == ']':
                    Translation.addWord(chinese, chinese2, english, "", file)
                    chinese = ""
                    chinese2 = ""
                    english = ""
                    other = ""
                    part1 = ""
                    status = 0
                else:
                    english += char
            if status == 4:
                if char == ']':
                    Translation.addWord(chinese, chinese2, english, other, file)
                    chinese = ""
                    chinese2 = ""
                    english = ""
                    other = ""
                    part1 = ""
                    status = 0
                else:
                    other += char

        if status == 1:
            logger.error(file.msg("只有`，没有译名，在行：%s" % line))
        if status == 2:
            logger.error(file.msg("没有译名没有结束。在行：%s" % line))

    @staticmethod
    def addWord(chinese, chinese2, english, other, file: ReadInfo):
        c = chinese.strip()
        c1 = c.replace("《", '')
        c = c1.replace("》", '')
        c1 = c.replace("”", '')
        c = c1.replace("“", '')
        c2 = chinese2.strip()
        c2 = c2.replace("(", '')
        c2 = c2.replace(")", '')
        c2 = c2.replace("（", '')
        c2 = c2.replace("）", '')
        e = english.strip()
        o = other.strip()
        if len(c) == 0:
            logger.error(file.msg("原名为空"))
            return
        if len(e) == 0:
            logger.error(file.msg("译名为空"))
            return
        if len(c2) == "":
            key = c
        else:
            key = c + ";(" + c2 + ")"
        if key in Translation.trWord.keys():  # 如果译名表中已经有这个中文
            data = Translation.trWord[key]
            if data.ifCache:  # 如果译名表的内容是旧的缓存
                if e != data.english:
                    logger.error(file.msg("译名与其他部分有岐义：%s '%s' 参考旧文件：%s %s %d '%s'" % (key, e,
                                                                                                     data.dirname,
                                                                                                     data.filename,
                                                                                                     data.lineno,
                                                                                                     data.english)))
                    return
                if len(o) != 0 and len(data.other) != 0 and data.other != o:
                    logger.error(file.msg("其他内容不同 %s '%s' 参考旧文件：%s %s %d '%s'" % (
                        key, o, data.dirname, data.filename, data.lineno, data.other)))
            else:
                if e != data.english:
                    logger.error(file.msg("译名有岐义：%s " % key))
                    return
                else:
                    if (not (file.filename == data.filename and file.lineno == data.lineno)
                            and (file.bookid == data.bookid)):
                        logger.error(file.msg("译名重复 %s 参考旧文件：%s %d" % (key, data.filename, data.lineno)))

            Translation.trWord[key].dirname = file.dirname
            Translation.trWord[key].filename = file.filename
            Translation.trWord[key].lineno = file.lineno
            Translation.trWord[key].other = o
            Translation.trWord[key].ifCache = False
        else:
            tr = Translation(c, c2, e, o, file)
            tr.ifCache = False
            Translation.trWord[key] = tr

    @staticmethod
    def OutputHtml(ifpdf):
        pyfirst = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'W',
                   'X', 'Y', 'Z']
        pydata = {}
        for f in pyfirst:
            pydata[f] = {}

        out = ""
        if ifpdf:
            out += "<style>.translation{font-size:9px;text-indent: 0px;}" \
                   ".pinyin{font-size:12px;font-weight:bold}" \
                   "</style>\n"
        else:
            out += "<style>.translation{font-size:10px;text-indent: 0px;}" \
                   ".pinyin{font-size:14px;font-weight:bold}" \
                   "</style>\n"
        out += "<table  border=0 class=translation>\n"

        keys = list(Translation.trWord.keys())
        keys.sort(key=cmp_to_key(compinyin))
        for key in keys:
            if Translation.trWord[key].ifCache:
                continue
            f = Translation.firstPinyin(key)
            if f in pyfirst:
                pydata[f][key] = Translation.trWord[key]
            else:
                logger.error("奇怪 %s %s" % (key, f))

        for f in pyfirst:
            dictlist = pydata[f]
            if len(dictlist.keys()) != 0:
                out += "<tr><td colspan=5 class=pinyin>%s</td></td><tr>\n" % f
                i = 0
                tr1 = None
                for key in dictlist.keys():
                    if i == 0:
                        tr1 = dictlist[key]
                        i = 1 - i
                    elif i == 1:
                        tr2 = dictlist[key]
                        out += Translation.WordHtml(tr1, tr2)
                        i = 1 - i
                        tr1 = None
                if tr1 is not None:
                    out += Translation.WordHtml(tr1, None)

        out += "</table>\n"

        return out

    @staticmethod
    def WordHtml(tr1, tr2):
        c1 = tr1.chinese
        c12 = tr1.chinese2
        e1 = tr1.english
        if len(c12) == 0:
            d1 = e1
        else:
            d1 = "(%s) %s" % (c12, e1)
        if tr2 is not None:
            c2 = tr2.chinese
            c22 = tr2.chinese2
            e2 = tr2.english
            if len(c22) == 0:
                d2 = e2
            else:
                d2 = "(%s) %s" % (c22, e2)
            return "<tr><td>%s</td><td>%s</td><td></td><td>%s</td><td>%s</td><tr>\n" % (c1, d1, c2, d2)
        else:
            return "<tr><td>%s</td><td>%s</td><td></td><td></td><td></td><tr>\n" % (c1, d1)
