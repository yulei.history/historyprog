from modules.config import logger
from modules.fileinfo import FileInfo
from modules.media import Media, MediaUrl, Index
from modules.toc import Toc


class Music(Media):

    def __init__(self, title='', indexes=None):
        super().__init__(indexes)
        self.MediaTitle = "音乐"
        self.media_note = "&#x266B;"
        self.media_type = 'music'
        self.media_label = "m"
        self.title = title
        self.linestr = ''

    def addIndex(self, chapter, index, tag=''):
        self.indexes.append(Index(chapter, index))

    def setData(self, linestr, chapter):
        data = linestr.split('|')
        if len(data) < 2:
            logger.error(self.file.msg("音乐信息不正常"))
            return None
        title = data[0]
        del data[0]
        musicurls = []
        for md in data:
            if ";" in md:
                mdata = md.split(';', 1)
                musicd = MediaUrl(mdata[0], mdata[1])
            else:
                musicd = MediaUrl('', md)
            musicurls.append(musicd)

        self.title = title
        self.urls = musicurls
        self.linestr = linestr

    def outputToc(self, mediano, chapter):
        if self.fileformat == 'pdf':
            rtn = '<h9 class="civ_hidden" tag="music" id="a_music%d">music</h9>' \
                  '<a href="{{{music.html}}}#c_music%d" >&#x266B;%d</a>' % (
                      mediano, self.contentsIndex, mediano)
            # h9这个隐藏标签目的是生成 toc文件，第二次生成PDF时，读取toc文件生成页码
        else:
            rtn = '<a href="javascript:open_m(\'a_music%d\', \'music%d\')" id="a_music%d" >&#x266B;%d</a>' % (
                mediano, mediano, mediano, mediano)
        return rtn

    @staticmethod
    def count(media_type='music'):
        return Media.count('music')

    @staticmethod
    def GetPageEnd(chapter, file: FileInfo):
        rtn = ''
        if not 'music' in Media.ContentArray.keys():
            return rtn
        for media in Media.ContentArray['music'].Contents:
            if media.inChapter(chapter):
                urlstr = ''
                title = media.title
                tagindex = -1
                htmlstr = ""
                for url in media.urls:
                    if len(url.title) == 0:
                        urlstr += '<li><a href="%s" target="_blank">%s</a></li>' % \
                              (url.url, url.url)
                    else:
                        urlstr += '<li><a href="%s" target="_blank">%s</a></li>' % \
                              (url.url, url.title)
                for index in media.indexes:
                    if index.chapter == chapter:
                        tagindex = media.contentsIndex
                    htmlstr += '<div id="music%d" class="popup_m">' \
                               '<p align=center> <a href="javascript:close_m(\'music%d\')">关闭窗口</a><br/> </p>' \
                               '<p><sup>&#x266B;%d</sup> %s<div><ul>' \
                               '%s' \
                               '</ul></div></p></div>' \
                               % (tagindex, tagindex, tagindex, title, urlstr)
                if tagindex == -1:
                    logger.error(file.msg("这个音乐没有使用者 %s" % title))
                    continue
                rtn += htmlstr

        return rtn

    # 输出音乐列表中youtube的列表
    @staticmethod
    def WriteMusicListYt(outputdir):
        rtn = ''
        for media in Music.contents('music'):
            yturl = ''
            urltitle = ''
            for url in media.urls:
                if "youtu" in url.url or "youtube" in url.url:
                    yturl = url.url
                    urltitle = url.title.replace('(y)', '')
                    if len(urltitle) != 0:
                        urltitle = '.' + urltitle

            for index in media.indexes:
                page = Toc.tocpage("a_music%d" % index.index)
                if len(page) != 0:
                    page = "(p%s)" % page
                title = "%d-%s%s%s" % (index.index, media.title, urltitle, page)
                title = title.replace(' ', '_').replace(':', '_')
                rtn += "%s:%s\n" % (title, yturl)
        if len(rtn) != 0:
            filename = outputdir + '/ytlist.txt'
            with open(filename, "w") as f:
                f.write(rtn)
            f.close()

    @staticmethod
    def GetMusicContent(fileformat):
        """
        输出目录页
        :param fileformat:
        :return:
        """

        if fileformat == 'pdf':
            rtn = """<style>
    h1 {
      text-align: center;
      font-size: 20px;
      font-family: arial;
    }
    span {float: right;}
    li {list-style: none;}
    ul {
      font-size: 12px;
      font-family: arial;
      margin-bottom: 3px;
      }
    ul {padding-left: 0em;}
    .toca {text-decoration:none; color: black;}
    .sourceurl {
      font-size: 10px;
      padding-left: 2em;
      word-wrap:break-word;
    }    
    </style>"""
        else:
            rtn = ''
        rtn += '<ul>\n'
        musicindex = 1
        for media in Music.contents('music'):
            labels = []
            for index in media.indexes:
                labels.append('<a href="c%s.html#a_music%d" class="toca">&#x266B;%d</a>' % \
                              (index.chapter, index.index, index.index))
            if fileformat == 'pdf':
                pages = []
                for index in media.indexes:
                    pages.append(Toc.tocpage("a_music%d" % index.index))
                page = ",".join(pages)
            else:
                page = ""
            label = " ".join(labels)
            rtn += '<li class="civ_musiccontent" id="c_music%d" >%s' \
                   '&nbsp;%s <span>%s</span>\n' % (
                       musicindex, label, media.title, page)
            rtn += '<ul><font size=-1>\n'
            for url in media.urls:
                rtn += '<li class="sourceurl">%s:&nbsp;<a href="%s" target="_blank" class="toca">%s</a></li>\n' % (
                    url.title, url.url, url.url)
            rtn += '</font></ul></li>\n'
            musicindex += 1
        rtn += '</ul>\n'
        return rtn

    def ProcessToc(self, datastr1, chapter, fileformat, file: FileInfo):
        rtn = super().ProcessToc(datastr1, chapter, fileformat, file)
        if len(rtn) > 0:
            return '<sup>' + rtn + '</sup>'
        else:
            return ''


def ProcessMusic(datastr, chapter, fileformat, file: FileInfo):
    music = Music()
    return music.ProcessToc(datastr, chapter, fileformat, file)


def GetPageEnd(chapter, file: FileInfo):
    return Music.GetPageEnd(chapter, file)


def GetMusicContent(fileformat):
    music = Music()
    return music.GetContent(fileformat)
