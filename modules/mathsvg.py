import json
import os
import subprocess

from modules.config import logger


class MathSVG:
    __Data = {}

    @staticmethod
    def reset():
        MathSVG.__Data = {}

    @staticmethod
    def readCache(outputdir):
        filename = outputdir + '/mathsvg.json'
        if os.path.exists(filename):
            with open(filename, "r") as f:
                jsondata = f.read()
            MathSVG.__Data = json.loads(jsondata)
            f.close()
        else:
            MathSVG.__Data = {}

    @staticmethod
    def writeCache(outputdir):
        filename = outputdir + '/mathsvg.json'
        with open(filename, "w") as f:
            f.write(json.dumps(MathSVG.__Data, sort_keys=True, indent=4))
        f.close()

    # 处理数学公式
    @staticmethod
    def process(datastr, mathtype, fileformat):
        if datastr in MathSVG.__Data.keys():
            result = MathSVG.__Data[datastr]
        else:
            if mathtype == 'tex':
                cmd = 'tex2svg.js --inline --speech false '
            else:
                cmd = 'am2svg.js --speech false '
            p = subprocess.Popen("node --no-deprecation %s '%s'" % (cmd, datastr), stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE, shell=True)
            out, err = p.communicate()
            result = out.decode()  # .replace('index', '').replace('radical', '')
            if err:
                logger.error("MATH convert '%s'" % datastr)
                logger.error(err.decode())
            else:
                MathSVG.__Data[datastr] = result
        if fileformat == 'pdf':
            return '<span style="font-size:0.71em" >%s</span>' % result
        else:
            return result
