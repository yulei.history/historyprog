from enum import Enum


class FileInfo:

    def __init__(self, filename, lineno=0):
        self.filename = filename
        self.lineno = lineno
        self.verboseDebug = False

    def __str__(self):
        return "filename:%s, lineno:%d" % (self.filename, self.lineno)

    def msg(self, message):
        return "%s(%d): %s " % (self.filename, self.lineno, message)

    def newline(self):
        self.lineno += 1


class LineStatus(Enum):
    Normal = 1  # 普通状态
    Para = 2  # 在段落中
    Quote = 3  # 在引用中
    Table = 4  # 在表格中


class ReadInfo(FileInfo):
    def __init__(self, dirname, filename, bookid):
        super().__init__(filename, 0)
        self.dirname = dirname
        self.lineStatus = LineStatus.Normal
        self.brace = 0  # 大括号状态，如果是0 ，在大括号外面，如果大于0, 表明是大括号的嵌套数。
        self.bookid = bookid

    def newfile(self, dirname, filename, bookid):
        self.dirname = dirname
        self.filename = filename
        self.lineno = 0
        self.lineStatus = LineStatus.Normal
        self.brace = 0
        self.bookid = bookid

    def lineStatus(self, lineStatus: LineStatus):
        self.lineStatus = lineStatus


class LineInfo(ReadInfo):
    line = ''

    def __init__(self, readinfo: ReadInfo, line):
        super().__init__(readinfo.dirname, readinfo.filename, readinfo.bookid)
        self.lineno = readinfo.lineno
        self.line = line
