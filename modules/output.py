from pypdf import PdfWriter

from modules import pages
from modules.config import logger
from modules.toc import Toc
from modules.translate import Translation
from modules.work import Work


class PageOutput:
    data = []

    def __init__(self, fileformat='html'):
        self.fileformat = fileformat
        self.filename = ''
        self.fullfilename = ''
        self.buffer = []
        self.prefilename = ''
        self.nextfilename = ''

    @staticmethod
    def dump():
        i = 0
        l = len(PageOutput.data)
        for output in PageOutput.data:
            outputfile_utf8 = open(output.fullfilename, 'w', encoding='utf8')
            if i == 0:
                prepage = ""
            else:
                prepage = '<a href="%s">上一章</a>&nbsp;|&nbsp;' % PageOutput.data[i - 1].filename
            if i >= l - 1:
                nextpage = ""
            else:
                nextpage = '&nbsp;|&nbsp;<a href="%s">下一章</a>' % PageOutput.data[i + 1].filename
            for line in output.buffer:
                line = line.replace('{{{prepage}}}', prepage)
                line = line.replace('{{{nextpage}}}', nextpage)
                print(line, file=outputfile_utf8)
            outputfile_utf8.close()
            i = i + 1


class Output:

    def __init__(self, work, fileformat='html'):
        self.work = work
        self.fileformat = fileformat
        self.currentPage = None
        if not work is None:
            self.attribs = work.attribs
        else:
            self.attribs = None
        self.translation = Translation.OutputHtml(self.fileformat == "pdf")

    # 输出一行至文件
    def write(self, text, encode='all'):
        # 输出text至文件
        #print(text, file=self.outputfile_utf8)
        if self.currentPage is None:
            logger.error("尚未设置输出文件")
            quit()
        self.currentPage.buffer.append(text)

    # 打开文件
    def open(self, outputdirname, filename, totalch, chapter=0, needHead=True):
        self.currentPage = PageOutput(self.fileformat)
        if not self.work is None:
            attrib = self.work.attribs
            title = attrib['title']
        else:
            title = pages.globaltitle
        outputfilename = outputdirname + '/' + filename
        self.currentPage.fullfilename = outputfilename
        self.currentPage.filename = filename
        logger.debug('Output to file: ' + outputfilename)
        #self.outputfile_utf8 = open(outputfilename, 'w', encoding='utf8')
        self.currentPage.buffer = []
        self.write('<!DOCTYPE html">')
        self.write('<HTML>')
        self.write('<HEAD>')
        self.write('<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">', 'utf8')
        if self.fileformat == 'pdf':
            self.write('<link href="civstylepdf.css" rel="stylesheet" media="screen">')
            self.write('<title>' + title + '</title>')
        else:
            if chapter != 0 and totalch >= chapter:
                logger.debug("work: %s. chapter: %d" % (self.work.debug(True), chapter))
                chapterTitle = self.work.chapters[chapter - 1].title
                self.write('<title>' + title + ' —— ' + chapterTitle + '</title>')
            else:
                self.write('<title>' + title + '</title>')
            self.write('<link href="civstyle.css" rel="stylesheet" media="screen">')
        if self.fileformat != 'pdf':
            self.write("""<script>
    function open_m(v, id){
        v1 = document.getElementById(v);
        pop = document.getElementById(id);
        w =  document.getElementById('civ_main').offsetWidth;
        h =  document.getElementById('civ_main').offsetHeight;
        idleft = v1.offsetLeft - 200;
        idtop = v1.offsetTop - 20;
        if ( idleft < 5 ) {
            idleft = 5
        }
        if ( idleft > w - 410 ) {
            idleft = w - 410
        }
        pop.style.left = idleft;
        pop.style.top = idtop;
        pop.style.display ='block';
    }
    function close_m(id){
        document.getElementById(id).style.display ='none';
    }
    </script>"""
                       )
        self.write('<!-- Sitemap 1.0 -->')
        self.write('</HEAD>')
        self.write('<BODY>')
        if needHead:
            if not self.work is None:
                self.head(chapter, totalch, self.work.chapters[chapter - 1].chapterNum == -1)
            else:
                self.head(chapter, totalch, True)

    # 关闭文件
    def close(self):
        if self.fileformat != 'pdf':
            self.write('<div id="fade" class="black_overlay"></div>')
        self.write('</BODY></HTML>')
        PageOutput.data.append(self.currentPage)
        self.currentPage = None
        # if not (self.outputfile_utf8 is None):
        #     self.outputfile_utf8.close()
        #     self.outputfile_utf8 = None

    # 输出目录（仅限HTML）
    def tc(self, ifWrite=True):
        tochtml = ''
        ch = 0
        bookid = self.work.bookid
        for c in self.work.chapters:
            # tc = c.tcindex
            for tc in c.tcindex:
                if tc['level'] == 1:
                    ch += 1
                    if tc['text'] != '目录':
                        if self.fileformat == 'html':
                            htmlfile = "c%s-%02d.html" % (bookid, ch)
                        else:
                            htmlfile = "c%s.html" % bookid  #其实没有意义，这个函数只有HTML才用到
                        tochtml = (tochtml + '<p class="civ_toc"><a href="%s" >%s</a></p>' %
                                   (htmlfile, tc['text']) + "\n")
        if ifWrite:
            self.write(tochtml)
        return tochtml
        # if Music.count() != 0:
        #     ch += 1
        #     self.write('<p class="civ_toc"><a href="c%02d.html" >音乐目录</a></p>' % ch)
        # if HImage.count('image') != 0:
        #     ch += 1
        #     self.write('<p class="civ_toc"><a href="c%02d.html" >插图目录</a></p>' % ch)
        # if HImage.count('map') != 0:
        #     ch += 1
        #     self.write('<p class="civ_toc"><a href="c%02d.html" >地图目录</a></p>' % ch)
        # if hasTranslation:
        #     ch += 1
        #     self.write('<p class="civ_toc"><a href="c%02d.html" >中英文译名对照表</a></p>' % ch)

    # 输出HTML的头部
    def head(self, chapter, totalch, noside=False):
        if not self.attribs is None:
            title = self.attribs['title']
        else:
            title = pages.globaltitle
        if self.fileformat == "pdf":
            return
        else:
            if not self.work is None:
                chapterTitle = self.work.chapters[chapter - 1].title
            else:
                chapterTitle = None
            self.write('<aside>')
            self.write('<header class="civ_header">')
            if chapter != 0 and totalch >= chapter:
                self.write(
                    '<div class="civ_title">' + title + ' —— ' + chapterTitle + '</div>')
            else:
                self.write('<div class="civ_title">' + title + '</div>')
            self.write('<div class="civ_nav">')
            self.write('{{{prepage}}} <a href="index.html">目录</a> {{{nextpage}}}')
            self.write('</header>')
            self.write('<footer class="civ_footer">')
            if not self.attribs is None:
                self.write('<div class="civ_title">' + self.attribs['version'] + '</div>')
            else:
                self.write('<div class="civ_title"></div>')
            self.write('<div class="civ_nav">')
            self.write('{{{prepage}}} <a href="index.html">目录</a> {{{nextpage}}}')
            self.write('</footer>')
            if chapter != 0 and totalch >= chapter and not noside:
                self.write('<section class="civ_sidebar  civ_tcindex">')
                self.write(self.indexhtmls[chapter])
                self.write('</section>')
            self.write('</aside>')

    # 输出底部
    def foot(self, chapter, footnotes):
        html = ""
        for foot in footnotes:
            if foot['chapter'] == chapter:
                html += foot['html'] + "\n"
        if len(html) > 0:
            self.write('<hr />')
            self.write(html)

    # 输出正文，PDF的处理方式与HTML不同
    def write2(self, c, chapter):
        if self.fileformat == "pdf":
            tcindex = self.indexhtmls[chapter]
            htmlstr = c.replace('{{{tcindex}}}', tcindex, 1)
            if Work.musicChapter != '':
                musichtml = "c%s.html" % Work.musicChapter
                htmlstr = htmlstr.replace('{{{music.html}}}', musichtml)
        else:
            htmlstr = c.replace('{{{tcindex}}}', '', 1)
        htmlstr = htmlstr.replace('{{{translation}}}', self.translation)
        self.write(htmlstr.replace('<p class="civ_content"></p>', ''))

    # 制作章节内的目录
    def makeTC(self):
        indexhtmls = []  # 章节内目录的HTML（下标是章节号）
        for i in range(0, 100):
            indexhtmls.append('<div class="civ_tcindex">')
        level = 0
        maxlevel = 0
        lastch = 1
        lastlevel = 0
        # TCIndexs = self.work.TCIndexs
        # for tc in TCIndexs:
        ch = 0
        for c in self.work.chapters:
            ch += 1
            for tc in c.tcindex:
                if tc['level'] == 1:
                    continue
                if lastch != ch:
                    while lastlevel > 0:
                        indexhtmls[lastch] = indexhtmls[lastch] + '    ' * (lastlevel - 1) + "</ul>\n"
                        lastlevel = lastlevel - 1
                    indexhtmls[lastch] = indexhtmls[lastch] + "</div>\n"
                    level = 0
                if tc['level'] > level:
                    while level < tc['level']:
                        indexhtmls[ch] = indexhtmls[ch] + '    ' * level + "<ul>\n"
                        level = level + 1
                        if level > maxlevel:
                            maxlevel = level
                            logger.debug(' Max level :' + str(maxlevel))
                if tc['level'] < level:
                    while level > tc['level']:
                        indexhtmls[ch] = indexhtmls[ch] + '    ' * (level - 1) + "</ul>\n"
                        level = level - 1
                if self.fileformat == 'pdf':
                    # indexhtmls[ch] = indexhtmls[ch] + '    ' * level + '<li>' + tc[
                    #     'text'] + "</li>\n"
                    tocpage = Toc.tocpage(tc['anchor'])
                    indexhtmls[ch] = indexhtmls[ch] + '    ' * level + '<li><a class="civ_toca" href="#' + tc[
                        'anchor'] + '">' + \
                                     tc[
                                         'text'] + "&nbsp;" + tocpage + "&nbsp;</a></li>\n"
                else:
                    indexhtmls[ch] = indexhtmls[ch] + '    ' * level + '<li><a href="#' + tc['anchor'] + '">' + tc[
                        'text'] + "</a></li>\n"
                lastlevel = level
                lastch = ch
            while lastlevel > 0:
                indexhtmls[lastch] = indexhtmls[lastch] + '    ' * (lastlevel - 1) + "</ul>\n"
                lastlevel = lastlevel - 1
            indexhtmls[lastch] = indexhtmls[lastch] + "</div>\n"
            self.indexhtmls = indexhtmls

    # def attrib(self, outattribfile):
    #     # 输出属性表
    #     attribfile_utf8 = open(outattribfile, 'w', encoding='utf8')
    #     print('#!/bin/bash', file=attribfile_utf8)
    #     print('declare -A bookattribs', file=attribfile_utf8)
    #     attribs = self.attribs
    #     for key in attribs:
    #         print('bookattribs[%s]="%s"' % (key, attribs[key]), file=attribfile_utf8)
    #     print('TITLE="%s"' % attribs['title'], file=attribfile_utf8)
    #     print('VERSION="%s"' % attribs['version'], file=attribfile_utf8)
    #     # if 'MATHSCRIPT' in Attrib.keys():
    #     #     print('MATHSCRIPT="%s"' % Attrib['MATHSCRIPT'], file=attribfile_utf8)
    #     attribfile_utf8.close()

    @staticmethod
    def Content(outputs):
        rtn = """<style>
    h1 {
      text-align: center;
      font-size: 20px;
      font-family: arial;
    }
    span {float: right;}
    li {list-style: none;}
    div {border-bottom: 1px dashed rgb(200,200,200);}
    ul {
      font-size: 14px;
      font-family: arial;
      margin-bottom: 3px;
      }
    ul ul {font-size: 80%; }
    ul {padding-left: 0em;}
    a {text-decoration:none; color: black;}
    .level-2{ text-indent: 1em;}
    </style>
    <ul>"""
        for bookoutput in outputs:

            ch = 0
            firstpage = ''
            tochtml = ''
            bookid = bookoutput.work.bookid
            for c in bookoutput.work.chapters:
                # tc = c.tcindex
                for tc in c.tcindex:
                    if tc['level'] == 1:
                        ch += 1
                        if tc['text'] != '目录':
                            tochtml += """<li class="level-2">
                            <div>
                            <a href="c%s-%02d.html#%s" name="n-%s">%s</a><span>%s</span>
                            </div>
                            """ % (bookid, ch, tc['anchor'], tc['anchor'], tc['text'], Toc.tocpage(tc['anchor']))
                            if len(firstpage) != 0:
                                firstpage = Toc.tocpage(tc['anchor'])

            rtn += """<li class="level-1">
            <div>
            <a href="c%s-00.html#tocbook-%s" name="c-%s">%s</a><span>%s</span>
            </div>
            """ % (bookid, bookid, bookid, bookoutput.work.attrib('title'), Toc.tocpage('tocbook-%s'%bookid))
            rtn += tochtml
        return rtn

    @staticmethod
    def PdfOutline(pdffile, outfile, outputs):
        logger.debug("read %s, write %s" % (pdffile, outfile))
        writer = PdfWriter(clone_from=pdffile)
        pagen = 0
        if len(Toc.tocpage('h_toc')) != 0:
            writer.add_outline_item('目录', int(Toc.tocpage('h_toc'))-1, is_open=False)
        for bookoutput in outputs:
            ch = 0
            outline = [None, None, None, None, None, None]
            lastlevel = 0
            for c in bookoutput.work.chapters:
                for tc in c.tcindex:
                    if len(Toc.tocpage(tc['anchor'])) == 0:
                        logger.error( "此标题没有页码 %s %s" %(tc['text'], tc['anchor']))
                    else:
                        pagen = int(Toc.tocpage(tc['anchor'])) - 1
                    if ch == 0:
                        outline[0] = writer.add_outline_item(bookoutput.work.attrib('title'), pagen, is_open=False)
                    level = tc['level']
                    if level > lastlevel and level - lastlevel > 1 :
                        logger.error("目录标题层次跨越：%s 当前级别：%d 前一个级别%d" % (tc['text'], level, lastlevel))
                    lastlevel = level
                    outline[level] = writer.add_outline_item(tc['text'], pagen,  parent=outline[level - 1], is_open=False)
                    ch += 1
        with open(outfile, "wb") as f:
            writer.write(f)
