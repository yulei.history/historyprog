# -*- coding: utf-8 -*-

import xml.etree.ElementTree as ET


class Toc:
    __tocdata = {}

    @staticmethod
    def reset():
        __tocdata = {}

    @staticmethod
    def readTocXml(tocxmlfile):
        tree = ET.ElementTree(file=tocxmlfile)
        root = tree.getroot()
        Toc._readXml(root)

    @staticmethod
    def tocpage(tockey):
        if tockey in Toc.__tocdata.keys():
            return Toc.__tocdata[tockey]
        else:
            return ''

    @staticmethod
    def _readXml(et):
        attribs = et.attrib
        if 'id' in attribs.keys() and 'page' in attribs.keys():
            id = attribs['id']
            page = attribs['page']
            if id != '' and page != '':
                Toc.__tocdata[id] = page
        for subet in et:
            Toc._readXml(subet)
        # for id in Toc.__tocdata.keys():
        #     print("%s: %s"%(id, Toc.__tocdata[id]))
