import os
import re

from modules import music, pages
from modules.config import logger
from modules.fileinfo import FileInfo, ReadInfo, LineStatus, LineInfo
from modules.footnote import ProcessFootnote
from modules.himage import HImage
from modules.mathsvg import MathSVG
from modules.media import Media
from modules.toc import Toc
from modules.translate import Translation


class Chapter:
    content = ''  # 读入文件的全部文本
    title = ''  # 章的标题
    tcindex = []  # 文件的目录信息
    indexhtml = ''  # 章节内目录的HTML（下标是章节号）
    chapterNum = 0  # 章的显示编号， 叵是无编号的，则设置为-1
    lines = []  # 全部的行的数据（LineInfo)

    def __init__(self, title='', chapterNum=-1, workfilename='', chapterLabel=''):
        self.content = ''  # 读入文件的全部文本
        self.title = title  # 章的标题
        self.label = chapterLabel # 章的标识文本，比如10-02
        self.tcindex = []  # 文件的目录信息
        self.indexhtml = ''  # 章节内目录的HTML（下标是章节号）
        self.chapterNum = chapterNum  # 章的显示编号， 叵是无编号的，则设置为-1
        self.lines = []  # 全部的行的数据（LineInfo)
        self.indexhtmls = []
        self.chatperstart = 0
        self.workfilename = workfilename


class Work(ReadInfo):
    htmltag = re.compile("<.*>")
    svgtag = re.compile("<svg.*>")
    bracetag = re.compile("{.*}")
    musicChapter = ''  # 如果包含音乐列表，记录音乐列表所在的章

    def __init__(self, fileformat, book=''):
        super().__init__("", "", book)
        self.localcachedir = '/tmp'
        self.indexhtmls = None
        self.chapters = []  # Chapter类的数组，全部的章节数据
        self.chapter = None

        self.attribs = {'title': "无标题", 'version': "版本未知"}  # 文档属性，(Title,Version)

        self.chapterstart = 0  # 起始章的编号

        self.fileformat = fileformat
        self.chapterNum = 0  # 全文章节号(显示的）
        self.tcindex = 0
        Work.musicChapter = ''  # 如果包含音乐列表，记录音乐列表所在的章

    def debug(self, ifRtn=False):
        msg = 'title: %s chapters: %d' % (self.attribs['title'], len(self.chapters))
        if ifRtn:
            return msg
        else:
            logger.debug(msg)

    def attrib(self, key, value=None):
        if key in self.attribs.keys():
            oldvalue = self.attribs[key]
        else:
            oldvalue = None
        if value is not None:
            self.attribs[key] = value
        return oldvalue

    # 处理上标和下标
    @staticmethod
    def processSupSub(datastr, fileinfo: FileInfo):
        inSub = False
        inSup = False
        inMath = False
        inMath1 = False
        htmlstr = ''
        substr = ''
        ci = 0
        while ci < len(datastr):
            char = datastr[ci]
            if not inSub and not inSup and not inMath and not inMath1:
                if char == '~':
                    inSub = True
                elif char == '^':
                    inSup = True
                elif char == '`':
                    inMath = True
                elif char == '\\' and datastr[ci + 1] == '(':
                    inMath1 = True
                else:
                    htmlstr += char
            elif inSub:
                if char == '~':
                    if len(substr) > 0:
                        htmlstr += '<sub>' + substr + '</sub>'
                    substr = ''
                    inSub = False
                elif char == '^':
                    inSub = False
                    inSup = True
                    htmlstr += '~' + substr
                    substr = ''
                else:
                    substr += char
            elif inSup:
                if char == '^':
                    if len(substr) > 0:
                        htmlstr += '<sup>' + substr + '</sup>'
                    substr = ''
                    inSup = False
                elif char == '~':
                    inSup = False
                    inSub = True
                    htmlstr += '^' + substr
                    substr = ''
                else:
                    substr += char
            elif inMath:
                if char == '`':
                    htmlstr += '`' + substr + '`'
                    substr = ''
                    inMath = False
                else:
                    substr += char
            elif inMath1:
                if char == '\\' and datastr[ci + 1] == ')':
                    htmlstr += '\\' + substr + '\\'
                    substr = ''
                    inMath1 = False
                else:
                    substr += char
            ci = ci + 1

        if len(substr) > 0:
            if inSub:
                htmlstr += '~' + substr
            elif inSup:
                htmlstr += '^' + substr
            else:
                logger.debug(fileinfo.msg('奇怪的上下标问题'))

        return htmlstr

    @staticmethod
    def processHref(datastr):
        data = datastr.split('|', 2)
        if len(data) > 1:
            hrefhtml = '<a href="%s">%s</a>' % (data[0], data[1])
        else:
            hrefhtml = '<a href="%s">%s</a>' % (data[0], data[0])
        return hrefhtml

    @staticmethod
    # 处理表格
    def tableLine(line):
        if line.startswith('|'):
            line1 = line[1:]
        else:
            line1 = line
        if line1.endswith('|'):
            line2 = line1[0:-1]
        else:
            line2 = line1
        tds = line2.split("|")
        rtn = "<tr>"
        for t in tds:
            rtn += "<td>" + t + "</td>"
        rtn += "</tr>"
        return rtn

    # 判断是否空行，这里的空行是指经过各种处理后，空行，比如有个标题，但仍然算作空行
    @staticmethod
    def emptyLine(line):
        line3 = line
        line3 = Work.svgtag.sub("SVG", line3)
        line3 = Work.htmltag.sub("", line3)
        line3 = Work.bracetag.sub("", line3)
        line3 = line3.replace("\r", "")
        line3 = line3.replace("\n", "")
        line3 = line3.replace(chr(0xfeff), "")
        return len(line3.strip()) == 0

    def processTitle(self, bracestr, ifImage):

        level = 1
        noChapterNumber = False

        if bracestr.startswith("#!"):  # 隐藏章编号的章（比如目录、译名等
            datastr = bracestr[2:]
            titles = datastr.split('|', 1)
            noChapterNumber = True
        else:
            datastr = bracestr[1:].strip()
            for char in datastr:  # 取得#数目，也即标题的级数
                if char == '#':
                    if level > 5:
                        logger.error(self.msg("标题层次不可以大于5"))
                    else:
                        level += 1
            datastr1 = datastr[level - 1:].strip()
            titles = datastr1.split('|', 1)

        if level == 1 and len(titles) == 1:
            # 对于第一级的标题（章），无论如何都不隐藏，也即如果没有标题，则把隐匿标题作为显示标题
            titles.append(titles[0])

        chapternum = -1

        if len(titles) == 2:
            if len(titles[0]) == 0 and len(titles[1]) == 0:
                logger.error(self.msg("标题没有内容"))
            # 若写成 {### xxxx|} 或 {### |xxxx}这样的，代表{### xxxx|xxxx}
            if len(titles[0]) == 0:
                titles[0] = titles[1]
            if len(titles[1]) == 0:
                titles[1] = titles[0]

        if level == 1:
            if not noChapterNumber:  # 第一级标题，非隐藏章编号情况下，增加章编号
                self.chapterNum += 1
                chapternum = self.chapterNum + self.chapterstart
                titles[0] = "第%d章  %s" % (chapternum, titles[0])
                titles[1] = "第%d章  %s" % (chapternum, titles[1])

        level1 = level + 1
        if len(titles) == 1:  # 标题隐藏(只出现在目录中，不出现在正文中）
            if len(titles[0]) == 0:
                logger.error(self.msg("标题没有内容"))
            tchtml = '<h%d id=toc%s-%d class="civ_hidden">%s</h%d>' % (
                level1, self.bookid, self.tcindex, titles[0], level1)
        elif titles[0] == titles[1]:  # 显式标题与隐藏标题相同的情况下
            if ifImage:
                tchtml = '<p class="civ_content"> </p>'
            else:
                tchtml = ''
            tchtml += '<h%d id=toc%s-%d>%s</h%d>' % (level1, self.bookid, self.tcindex, titles[1], level1)
        else:  # 显式标题与隐藏标题不同的情况下
            if ifImage:
                tchtml = '<p class="civ_content"> </p>'
            else:
                tchtml = ''
            tchtml += '<h%d id=toc%s-%d tag="title">%s</h%d>' % (level1, self.bookid, self.tcindex, titles[1], level1)
            tchtml += '<h%d class="civ_hidden">%s</h%d>' % (level1, titles[0], level1)
        if level == 1:
            if not noChapterNumber:  # 隐藏章节号的章（如目录 ，译名表）
                tchtml += "\n{{{tcindex}}}\n"  # 一般的章 在开头处要增加小目录
            chaptertext = "%s-%02d" % (self.bookid, chapternum+1)
            self.chapter = Chapter(titles[0], chapternum, self.filename, chaptertext)
            self.chapters.append(self.chapter)

        # if titles[0] != 'translation':
        tc = {'level': level, 'text': titles[0], 'index': self.tcindex, 'chapter': self.chapterNum,
              'anchor': "toc%s-%d" % (self.bookid, self.tcindex)}
        self.chapter.tcindex.append(tc)
        self.tcindex += 1
        if level == 1:
            self.chapter.content = "<p class=civ_content>"
        return tchtml

    # 处理大括号
    def processBrace(self, bracestr, ifImage=False):
        datastr = bracestr[1:].strip()
        datastr1 = bracestr[1:]

        if bracestr.startswith("#"):  # 标题
            rtn = self.processTitle(bracestr, ifImage)
            return rtn, True, False
        chapterindex = len(self.chapters)
        chaptertext = "%s-%02d" % (self.bookid, chapterindex)
        if bracestr.startswith("&"):  # 地图
            return HImage.ProcessMap(datastr1, chaptertext, self.fileformat, self), True, True
        if bracestr.startswith("!"):  # 图片
            return HImage.ProcessImage(datastr1, chaptertext, self.fileformat, self), True, True
        if bracestr.startswith("*"):  # 超连接
            return Work.processHref(datastr), False, False
        if bracestr.startswith("m"):  # 音乐
            return music.ProcessMusic(datastr1, chaptertext, self.fileformat, self), False, False
        if bracestr.startswith("%"):  # 数学公式
            # Attrib['MATHSCRIPT'] = '--javascript-delay 10000 --no-stop-slow-scripts '
            if bracestr.startswith("%T") or bracestr.startswith("%t"):
                # haveMath[chapter] = haveMath[chapter] | 1
                return MathSVG.process(datastr[1:].strip(), 'tex', self.fileformat), False, False
            else:
                # haveMath[chapter] = haveMath[chapter] | 2
                return MathSVG.process(datastr, 'ascii', self.fileformat), False, False
        if bracestr.startswith("^"):  # 引用
            return ProcessFootnote(datastr1, chaptertext, self.fileformat, self), False, False
        if bracestr.startswith("$"):  # 属性
            (key, value) = datastr.split("=", 1)
            self.attrib(key.lower(), value)
            if key.lower() == 'chapterstart':
                self.chapterstart = int(value) - 1
                if self.chapterstart < 0:
                    self.chapterstart = 0
            return "", False, False
        if bracestr.lower().startswith("translation"):  # 译名对照表
            #return Translation.OutputHtml(self.fileformat == "pdf"), True
            return '{{{translation}}}', True, False
        if bracestr.lower().startswith("list"):  # 译名对照表
            (_, mediatype) = bracestr.split(" ")
            if mediatype == 'image' or mediatype == "map":
                media = HImage(mediatype)
                return media.GetContent(self.fileformat), True, False
            elif mediatype == 'music':
                Work.musicChapter = chaptertext
                return music.GetMusicContent(self.fileformat), True, False
            else:
                logger.error(self.msg("未知的媒体类型：%s") % mediatype)
                return '', True, False
        if bracestr.startswith("`"):  # 译名列表
            Translation.ProcessWord(datastr1, self)
            return "", True, False
        if bracestr == 'pagebreak':  # PDF的换页
            if self.fileformat == 'pdf':
                return '<div style="display:block; clear:both; page-break-after:always;"></div>', True, False
            else:
                return '', True, False
        # logger.debug("奇怪的：%s" % bracestr)
        return '', False, False

    def step1(self, inputfile):  # 1. 处理大括号，优先于其他所有
        bracestr = ''
        line0 = ''
        ifImage = False  # 之所以增加这个，是因为要解决以下问题：如果图片之后有显式标题，岀需要插入一段<p>
        for inputline in inputfile:
            self.newline()
            line = inputline.strip()
            if self.brace == 0:
                line0 = ''
            for char in line:
                if char == '{':
                    self.brace += 1
                    if self.brace > 1:
                        bracestr += char
                    continue
                if self.brace > 0:
                    if char == '}':
                        self.brace -= 1
                        if self.brace == 0:
                            result, newPara, ifImage = self.processBrace(bracestr.strip(), ifImage)
                            # 之所以引入newPara是因为某些（标题，插图）大括号处理时，必须视作一个新的段落
                            if newPara:
                                self.chapter.lines.append(LineInfo(self, line0))
                                self.chapter.lines.append(LineInfo(self, result))
                                self.chapter.lines.append(LineInfo(self, ''))
                                line0 = ''
                            else:
                                line0 += result
                            bracestr = ''
                        else:
                            bracestr += char
                    else:
                        bracestr += char
                else:
                    line0 += char
            if self.brace > 0:  # 如果换行时尚且在大括号中，直接继续，不进行下面的行处理
                continue
            if self.chapter is not None:
                self.chapter.lines.append(LineInfo(self, line0))
            else:
                logger.debug(self.msg(line0))

    def step2(self, line):
        line0 = line.line
        # logger.debug(line.msg(self.lineStatus))
        # logger.debug(line.msg(line0))
        # 2. 单行的第一遍处理
        # 2.1 处理空行
        if Work.emptyLine(line0):
            if self.lineStatus == LineStatus.Quote:  # 如果在引用中，遇到空行，引用结束
                line1 = "</blockquote>\n" + line0
                self.lineStatus = LineStatus.Normal
            elif self.lineStatus == LineStatus.Para:  # 如果在段落中，遇到空行，引用结束
                line1 = "</p>\n" + line0
                self.lineStatus = LineStatus.Normal
            elif self.lineStatus == LineStatus.Table:  # 如果在表格中，遇到空行，引用结束
                line1 = "</table>\n" + line0
                self.lineStatus = LineStatus.Normal
            else:
                line1 = line0
            return line1

        # 2.2 在引用中
        if self.lineStatus == LineStatus.Quote:  # 如果在引用中
            if line0.startswith('>'):  # 继续引用
                line1 = "<br />" + line0[1:]
            elif line0.startswith('|'):  # 遇到表格符号，进入表格
                line1 = "<table border=1 >\n"
                line1 += Work.tableLine(line0)
                self.lineStatus = LineStatus.Table
            else:  # 如果新的一行不是引用，则结束引用状态，进入段落
                line1 = '</blockquote>\n<p class="civ_content">' + line0
                self.lineStatus = LineStatus.Para

        # 2.3 在段落中
        elif self.lineStatus == LineStatus.Para:  # 如果在段落中
            if line0.startswith('>'):  # 出现引用符号，则结束段落，进入引用状态
                line1 = "</p>\n<blockquote>" + line0[1:]
                self.lineStatus = LineStatus.Quote
            elif line0.startswith('|'):  # 遇到表格符号，进入表格
                line1 = "<table  border=1>\n"
                line1 += Work.tableLine(line0)
                self.lineStatus = LineStatus.Table
            else:  # 否则，继续段落的状态，且加一个换行符
                line1 = "<br />\n" + line0

        # 2.4 在表格中
        elif self.lineStatus == LineStatus.Table:  # 如果在表格状态下
            if line0.startswith('>'):  # 若遇到引用符号，则结束表格，进入引用状态
                line1 = "</table>\n<blockquote>" + line0[1:]
                self.lineStatus = LineStatus.Quote
            elif line0.startswith('|'):  # 继续表格
                line1 = Work.tableLine(line0)
            else:  # 结束表格，进入段落状态
                line1 = '</table>\n<p class="civ_content">' + line0
                self.lineStatus = LineStatus.Para
        else:  # 在普通状态下：
            if line0.startswith('>'):  # 遇到引用，进入引用状态
                line1 = "<blockquote>" + line0[1:]
                self.lineStatus = LineStatus.Quote
            elif line0.startswith('|'):  # 遇到表格，进入表格状态
                line1 = "<table  border=1>\n"
                line1 += Work.tableLine(line0)
                self.lineStatus = LineStatus.Table
            else:  # 进入段落状态，非空行的情况下，有普通文字，都会进入段落状态
                line1 = '<p class="civ_content">' + line0
                self.lineStatus = LineStatus.Para

        #logger.debug(line.msg(line1))
        # 3. 处理句子中出现的译名
        line2 = ""
        tranStatus = 0  # 如果出现`，表明有译名需要处理
        inBrackets = 0  # 在中括号中，这个意味着，非翻译文本的中括号（也就是无`的[之后)，若此值为1,且下一个字符不是[，则出错
        for char in line1:
            if char == '`':
                tranStatus = 1
                Translation.ProcessWord(line1, self)
            elif char == '[' and tranStatus == 0 and inBrackets == 0:
                inBrackets = 1
                line2 += char
            elif char == '[' and tranStatus == 0 and inBrackets == 1:
                inBrackets = 0  # 连续的两个[  这时应该忽略一个[
            elif inBrackets == 1 and char != '[' and tranStatus == 0:
                logger.error(line.msg('译名一定有问题 %s' % line2))
                inBrackets = 0
                line2 += char
            elif char == ']':
                tranStatus = 0
                line2 += char
            else:
                line2 += char

        # 5. 处理上标下标
        line4 = Work.processSupSub(line2, self)

        return line4

    def readAll(self, inputdirname, workfiles, isLastBook):
        self.chapterNum = 0  # 全文章节号
        self.tcindex = 0

        for workfile in workfiles:
            inputfile = open(workfile, encoding='UTF-8-sig')

            self.newfile(inputdirname, os.path.basename(workfile), self.bookid)
            logger.debug(self.msg("开始处理文件"))
            self.step1(inputfile)

            inputfile.close()

        # 以下追加特殊页面：
        addpages = []
        if Media.count('image') != 0:
            addpages.append(pages.listimage)
        if Media.count('map') != 0:
            addpages.append(pages.listmap)
        if Media.count('music') != 0:
            addpages.append(pages.listmusic)
        addpages.append(pages.translate)

        if isLastBook:
            for page in addpages:
                self.step1(page.splitlines())

        for chapter in self.chapters:
            self.chapter = chapter
            for line in chapter.lines:
                self.filename = line.filename
                self.lineno = line.lineno
                line4 = self.step2(line)

                # logger.debug( line4 )
                if len(line4.strip()) != 0:
                    self.chapter.content += line4 + "\n"

            workfilename = chapter.workfilename + ".list"
            medialistfile = os.path.join(self.localcachedir, workfilename)
            medialist = Media.ExportTagContent("map", chapter.label) + "\n\n"
            medialist += Media.ExportTagContent("image", chapter.label) + "\n\n"
            medialist += Media.ExportTagContent("music", chapter.label) + "\n\n"
            medialist += Media.ExportTagContent("footnote", chapter.label) + "\n\n"
            open(medialistfile, 'w').write(medialist)



    # 制作章节内的目录
    def makeTC(self):
        indexhtmls = []  # 章节内目录的HTML（下标是章节号）
        for i in range(0, 100):
            indexhtmls.append('<div class="civ_tcindex">')
        level = 0
        maxlevel = 0
        lastch = 1
        lastlevel = 0
        # TCIndexs = self.work.TCIndexs
        # for tc in TCIndexs:
        ch = 0
        for c in self.chapters:
            ch += 1
            for tc in c.tcindex:
                if tc['level'] == 1:
                    continue
                if lastch != ch:
                    while lastlevel > 0:
                        indexhtmls[lastch] = indexhtmls[lastch] + '    ' * (lastlevel - 1) + "</ul>\n"
                        lastlevel = lastlevel - 1
                    indexhtmls[lastch] = indexhtmls[lastch] + "</div>\n"
                    level = 0
                if tc['level'] > level:
                    while level < tc['level']:
                        indexhtmls[ch] = indexhtmls[ch] + '    ' * level + "<ul>\n"
                        level = level + 1
                        if level > maxlevel:
                            maxlevel = level
                            logger.debug(' Max level :' + str(maxlevel))
                if tc['level'] < level:
                    while level > tc['level']:
                        indexhtmls[ch] = indexhtmls[ch] + '    ' * (level - 1) + "</ul>\n"
                        level = level - 1
                if self.fileformat == 'pdf':
                    # indexhtmls[ch] = indexhtmls[ch] + '    ' * level + '<li>' + tc[
                    #     'text'] + "</li>\n"
                    tocpage = Toc.tocpage(tc['anchor'])
                    indexhtmls[ch] = indexhtmls[ch] + '    ' * level + '<li><a class="civ_toca" href="#' + tc[
                        'anchor'] + '">' + tc['text'] + "&nbsp;" + tocpage + "&nbsp;</a></li>\n"
                else:
                    indexhtmls[ch] = indexhtmls[ch] + '    ' * level + '<li><a href="#' + tc['anchor'] + '">' + tc[
                        'text'] + "</a></li>\n"
                lastlevel = level
                lastch = ch
            while lastlevel > 0:
                indexhtmls[lastch] = indexhtmls[lastch] + '    ' * (lastlevel - 1) + "</ul>\n"
                lastlevel = lastlevel - 1
            #indexhtmls[lastch] = indexhtmls[lastch] + "</div>\n"
            self.indexhtmls = indexhtmls
