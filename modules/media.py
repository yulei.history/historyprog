from modules.config import logger
from modules.fileinfo import FileInfo
from modules.toc import Toc

"""
本程序用于处理文件的多媒体内容，目前包括图片、地图、音乐：

说明：
class Medias  是Images和Music、Footnote类的基类
    包含动态对象和静态数据，一个动态对象就是一个内容，静态数据则是全部的内容数据
class Index  是一个数据结构

名词：
内容，Content ：指单独一个媒体的信息
标签，Tag ： 使用{x &标签 内容} 来定义一个标签，而在 {x #标签}处使用这种标签的内容，其中x可以是音乐m,地图&或者图片！

"""


class MediaUrl:
    def __init__(self, title='', url=None):
        if url is None:
            url = ''
        self.title = title
        self.url = url


class Index:
    def __init__(self, chapter, index):
        self.chapter = chapter  # 本内容所属于的章
        self.index = index  # 本内容在Contents中的下标


class MediaContents:

    def __init__(self):
        self.Contents = []  # 全部的媒体内容信息，这个数组的下标将作为显示的下标
        self.Tags = {}  # 以标签为下标的内容信息


class Media:
    # 静态数据
    ContentArray = {}  # 这个是由MediaContents组成的哈希表，下标是media_type，值是MediaContents

    @staticmethod
    def reset():
        Media.ContentArray = {}

    def __init__(self, indexes=None):
        self.MediaTitle = "未命名"  # 这个是指定本对象的内容中文名，用于输入调试或者错误信息，继承的类应该重载它
        self.media_type = "unknown"  # 这个是指定本对象的内容类型，目前可以有image, map ,music，继承的类应该重载它
        self.media_note = "U"  # 媒体标识符号，分别是“插图” “地图” 和音乐符号，继承的类应该重载它
        self.media_label = "m"  # 音乐m 脚注^ 地图& 插图!
        self.urls = []

        self.data = None  # 存储内容数据，继承的类应该根据自己情况设置各自的数据
        self.indexes = []  # 每一个媒体可能被多次使用（仅限一章之内），本数组存储被使用的情况。
        self.contentsIndex = -1  # 当内容被加入到Contents数组里时，index指向它在数组中的位置
        self.file = None  # 当前正在处理的文件信息
        self.fileformat = "pdf"
        self.linestr = ''
        self.chapter = ''

        if indexes is None:
            self.indexes = []
        else:
            self.indexes = indexes
        self.contentsIndex = -1

        if not self.media_type in Media.ContentArray.keys():
            Media.ContentArray[self.media_type] = MediaContents()

    def addIndex(self, chapter, index, tag=''):
        if len(self.indexes) != 0:
            logger.error(self.file.msg("%s标签‘%s’被重复使用" % (self.MediaTitle, tag)))
        else:
            self.indexes.append(Index(chapter, index))

    def inChapter(self, chapter):
        for index in self.indexes:
            if chapter == index.chapter:
                return True
        return False

    def getMediaCount(self, chapter=''):
        if chapter == '':
            return len(self.getContents()) + 1
        else:
            rtn = 0
            if self.media_type in Media.ContentArray.keys():
                contents = Media.ContentArray[self.media_type].Contents
                for media in contents:
                    if len(media.indexes) > 0:
                        for index in media.indexes:
                            if index.chapter == chapter:
                                rtn += 1
                return rtn
            else:
                return 0
        # if self.media_type in ContentArray.keys():
        #     return ContentArray[self.media_type].MediaCount
        # else:
        #     return 1

    def getContents(self):
        if self.media_type in Media.ContentArray.keys():
            return Media.ContentArray[self.media_type].Contents
        else:
            return []

    @staticmethod
    def contents(media_type):
        if media_type in Media.ContentArray.keys():
            return Media.ContentArray[media_type].Contents
        else:
            return []

    def appendContent(self, media):
        if self.media_type in Media.ContentArray.keys():
            Media.ContentArray[self.media_type].Contents.append(media)
        else:
            Media.ContentArray[self.media_type] = MediaContents()
            Media.ContentArray[self.media_type].Contents.append(media)

    def getTags(self):
        if self.media_type in Media.ContentArray.keys():
            return Media.ContentArray[self.media_type].Tags
        else:
            return {}

    def setTag(self, tag, content):
        """
            将标签数据存储
        :param tag:  标签
        :param content:  数据对象本身（Media对象)
        :return:
        """
        if self.media_type in Media.ContentArray.keys():
            Media.ContentArray[self.media_type].Tags[tag] = content
        else:
            Media.ContentArray[self.media_type] = MediaContents()
            Media.ContentArray[self.media_type].Tags[tag] = content

    @staticmethod
    def count(media_type):
        return len(Media.contents(media_type))

    def setData(self, linestr, chapter):
        """
        从 TAG字符中获取数据，继承的类需要重载这个函数以获取实际的数据
        :param linestr:  Tag字符串
        :return: 返回的数据
        """
        self.linestr = linestr
        self.data = linestr

    def outputToc(self, mediano, chapter):
        """
        输出文章页面中的媒体表示。继承的害需要重载这个函数
        :param mediano:
        :return:
        """
        # if self.fileformat == 'pdf':
        #     rtn = '<h9 class="civ_hidden" tag="music" id="a_music%d">music</h9>' \
        #           '<a href="music.html#c_music%d" id="a_music%d" >&#x266B;%d</a>' % (
        #               mediano, self.contentsIndex, mediano, mediano)
        # else:
        #     rtn = '<a href="javascript:open_m(\'a_music%d\', \'music%d\')" id="a_music%d" >&#x266B;%d</a>' % (
        #         mediano, mediano, mediano, mediano)
        return ""

    def ProcessToc(self, datastr1, chapter, fileformat, file: FileInfo):
        """
            行文中处理 多媒体内容
        :param datastr1:  多媒体内容
        :param chapter: 第几部分第几章
        :param fileformat:  格式，pdf或html
        :param file:   当前文件
        :return:  返回需要插入当前文件行
        """
        self.file = file
        self.fileformat = fileformat
        rtn = ''
        datastr = datastr1.strip()
        if datastr.startswith("&") or datastr.startswith("!") or datastr.startswith("^") or datastr.startswith("m"):
            # 定义了标签和它的内容。
            (tag, linestr) = (datastr[1:]).split(" ", 1)
            tag1 = 'chapter%s_' % chapter + tag.strip()
            if tag1 in self.getTags().keys():
                logger.error(file.msg("%s标签 '%s' 出现重复" % (self.MediaTitle, tag1)))
            else:
                self.setData(linestr.strip(), chapter)
                self.setTag(tag1, self)
        elif not datastr1.startswith(" "):  # 根据标签，实际插入内容
            tag = datastr1.strip()
            tag1 = 'chapter%s_' % chapter + tag
            if tag1 in self.getTags().keys():
                media = self.getTags()[tag1]
                mediano = media.getMediaCount()  # 当前媒体总编号
                media.file = file
                media.addIndex(chapter, mediano, tag)
                if media.contentsIndex == -1:
                    media.contentsIndex = len(media.getContents()) + 1
                    self.appendContent(media)
                rtn = media.outputToc(mediano, chapter)
            else:
                logger.error(file.msg("找不到%s标签 '%s'" % (self.MediaTitle, tag1)))
        else:
            self.setData(datastr1.strip(), chapter)
            mediano = self.getMediaCount()
            self.addIndex(chapter, mediano)
            self.contentsIndex = len(self.getContents()) + 1
            self.appendContent(self)
            rtn = self.outputToc(mediano, chapter)
        self.file = None
        return rtn

    def GetContent(self, fileformat):
        """
        输出目录页
        :param fileformat:
        :return:
        """

        if not self.media_type in Media.ContentArray.keys():
            logger.debug(" '%s' 没有数据， 返回" % self.media_type)
            return ''

        if fileformat == 'pdf':
            rtn = """<style>
    h1 {
      text-align: center;
      font-size: 20px;
      font-family: arial;
    }
    span {float: right;}
    li {list-style: none;}
    ul {
      font-size: 12px;
      font-family: arial;
      margin-bottom: 3px;
      }
    ul {padding-left: 0em;}
    .toca {text-decoration:none; color: black;}
    .sourceurl {
      font-size: 10px;
      padding-left: 2em;
      word-wrap:break-word;
    }    
    </style>"""
        else:
            rtn = ''
        rtn += '<ul>\n'
        mediaindex = 1
        for media in Media.contents(self.media_type):
            labels = []
            for index in media.indexes:
                labels.append('<a href="c%s.html#a_%s%d" class="toca">%s%d</a>' % \
                              (index.chapter, self.media_type, index.index, self.media_note, index.index))
            if fileformat == 'pdf':
                pages = []
                for index in media.indexes:
                    pages.append(Toc.tocpage("a_%s%d" % (self.media_type, index.index)))
                page = ",".join(pages)
            else:
                page = ""
            label = " ".join(labels)
            rtn += '<li id="c_%s%d" >%s' \
                   '&nbsp;%s <span>%s</span>\n' % (
                       self.media_type, mediaindex, label, media.title, page)
            rtn += '<ul><font size=-1>\n'
            for url in media.urls:
                if len(url.title) != 0:
                    if len(url.url) != 0:
                        rtn += ('<li class="sourceurl">%s: <a href="%s" target="_blank" class="toca">%s</a></li>\n'
                                % (url.title, url.url, url.url))
                    else:
                        rtn += '<li class="sourceurl">%s</li>\n' % (
                            url.title)
                else:
                    if len(url.url) != 0:
                        rtn += '<li class="sourceurl"><a href="%s" target="_blank" class="toca">%s</a></li>\n' % (
                            url.url, url.url)
            rtn += '</font></ul></li>\n'
            mediaindex += 1
        rtn += '</ul>\n'
        return rtn

    @staticmethod
    def ExportTagContent(media_type, chapter):
        """
        输出媒体的标签列表，将一章中的媒体，自动安排标签，并列表输出
        :return:
        """

        if not media_type in Media.ContentArray.keys():
            logger.debug(" '%s' 没有数据， 返回" % media_type)
            return ''

        chapterindex = 10
        lines = []
        for media in Media.contents(media_type):

            if chapter != media.indexes[0].chapter:
                continue
            line = '{%s%s%d %s}' % (media.media_label, media.media_label, chapterindex, media.linestr )
            chapterindex += 10
            lines.append(line)
        return "\n".join(lines)

    # @staticmethod
    # def debug():
    #     print("===============================")
    #     for media_type in ContentArray.keys():
    #         media = ContentArray[media_type]
    #         contents = media.Contents
    #         print("-----------%s---start------------------" % media_type)
    #         for c in contents:
    #             print(c)
    #         print("-----------%s---end------------------" % media_type)
    #     print("===============================")
