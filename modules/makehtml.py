import os

from modules import music, pages
from modules.config import logger
from modules.fileinfo import FileInfo
from modules.footnote import Footnote
from modules.himage import HImage
from modules.mathsvg import MathSVG
from modules.media import Media
from modules.music import Music
from modules.output import Output, PageOutput
from modules.toc import Toc
from modules.translate import Translation
from modules.work import Work


class MakeHtml:

    def __init__(self, input_dir, workfile='', output_dir='output', globalcachedir='output/cache',
                 localcachedir='', tocfile='', fileformat='html'):
        self.inputdirname = input_dir
        self.outputdirname = output_dir
        #        self.attrib_file = ""
        self.workfile = workfile
        self.tocxmlfile = tocfile
        self.fileformat = fileformat
        self.error_exit = False
        self.attribs = None
        self.output = None
        self.globalcachedir = globalcachedir
        self.localcachedir = localcachedir
        self.lastchapter = 0

    def runAll(self, booklist, split='', splitabc='', startchapter = 0):

        Media.reset()
        Footnote.reset()
        HImage.reset()
        MathSVG.reset()
        Toc.reset()
        Translation.reset()

        if self.outputdirname.strip() == '':
            logger.error('输出目录不能为空')
            self.error_exit = True
            return

        if len(booklist) == 1:
            allinone = False
        else:
            allinone = True

        if not (os.path.exists(self.outputdirname)):
            os.makedirs(name=self.outputdirname, exist_ok=True)

        MathSVG.readCache(self.globalcachedir)
        Translation.readCache(self.globalcachedir)

        works = []
        outputs = []
        allch = 0
        for book in booklist:  # booklist其实只用于AllInOne模式，一般的情况下只有一个book
            bookdir = os.path.join(self.inputdirname, book)

            if not HImage.SetDirs(bookdir, self.outputdirname, self.localcachedir + "/", book):
                logger.error('无法设置图片目录')
                self.error_exit = True
                return

            inputdirfiles = os.listdir(bookdir)
            inputdirfiles.sort()
            workfiles = []
            if self.workfile == '':
                if book != '10' and not allinone and splitabc != 'b':
                    descript_work = os.path.join(self.inputdirname, '10', '000.work')
                    if os.path.isfile(descript_work):
                        workfiles.append(descript_work)

                if splitabc != 'b':
                    for f in inputdirfiles:
                        if f.endswith(".work") and not f.startswith("_"):
                            workfile = os.path.join(bookdir, f)
                            if f == split+'.work':
                                break
                            if os.path.isfile(workfile):
                                workfiles.append(workfile)

                else:
                    after = False
                    for f in inputdirfiles:
                        if f.endswith(".work") and not f.startswith("_"):
                            workfile = os.path.join(bookdir, f)
                            if f == split+'.work':
                                after = True
                            if os.path.isfile(workfile) and after :
                                workfiles.append(workfile)

            else:
                if not os.path.isfile(os.path.join(bookdir, self.workfile)):
                    logger.error('没有指定的work文件 %s' % self.workfile)
                    self.error_exit = True
                    return
                else:
                    workfiles.append(os.path.join(bookdir, self.workfile))

            if len(workfiles) == 0:
                logger.error('指定目录下没有work 文件 %s' % bookdir)
                self.error_exit = True
                return

            if self.tocxmlfile != '' and not os.path.isfile(self.tocxmlfile):
                logger.error('指定的toc xml 文件不存在')
                self.error_exit = True
                return

            if self.tocxmlfile != '':
                # 解析toc.xml文件
                Toc.readTocXml(self.tocxmlfile)

            work = Work(self.fileformat, book)
            if self.attribs is not None:
                work.attribs = self.attribs
            work.chapterstart = startchapter
            work.localcachedir = self.localcachedir

            workfiles.sort()

            work.readAll(bookdir, workfiles, book == booklist[-1])

            allch += len(work.chapters)
            work.makeTC()
            works.append(work)
            self.lastchapter = work.chapterNum

            output = Output(work, self.fileformat)
            output.makeTC()
            outputs.append(output)

        MathSVG.writeCache(self.globalcachedir)
        Translation.writeCache(self.globalcachedir)
        Music.WriteMusicListYt(self.localcachedir)

        # 输出从这里开始

        ofilename = 'index.html'
        output = Output(None, self.fileformat)
        output.open(self.outputdirname, ofilename, allch, 0)
        if self.fileformat == 'pdf':
            output.write("<section class=civ_main_pdf>")
        else:
            output.write("<section class=civ_main2>")
        output.write('<h1 class="civ_covertitle">%s<font size=2em>(暂定名)</font></h1>'%pages.globaltitle)
        output.write('<h3>作者：俞磊</h3>')
        if not allinone:
            if split != '':
                if splitabc != 'b':
                    titlea = '（上）'
                else:
                    titlea = '（下）'
            else:
                titlea = ''
            output.write('<h2>' + works[0].attrib('title') + titlea + '</h2>')
        if self.fileformat != 'pdf':
            output.write('<h2 id="h_toc" >目录</h2>')
            if not allinone:
                output.write(outputs[0].tc(False))
            else:
                for bookoutput in outputs:
                    output.write('<h2>' + bookoutput.work.attrib('title') + '</h2>')
                    output.write(bookoutput.tc(False))
        output.write("</section>")
        output.close()

        if self.fileformat == 'pdf':
            output.open(self.outputdirname, 'index1.html', allch, 0, False)
            output.write(pages.index1)
            output.close()
            output.open(self.outputdirname, 'toc.html', allch, 0, False)
            output.write('<h2 id="h_toc" >目录</h2>')
            output.write(Output.Content(outputs))
            output.close()

        for output in outputs:
            work = output.work
            chapter = 0

            for i in range(0, len(work.chapters)):
                fileinfo = FileInfo(i, 0)
                # for c in Contents:
                c = work.chapters[i].content

                # c = work.Contents[i]
                chapter += 1

                ofilename = 'c%s-%02d.html' % (work.bookid, chapter)
                output.open(self.outputdirname, ofilename, allch, chapter)
                if self.fileformat == 'pdf':
                    output.write('<section class=civ_main_pdf>')
                else:
                    if work.chapters[i].chapterNum == -1:
                        output.write("<section class=civ_main2 >")
                    else:
                        output.write("<section class=civ_main id='civ_main'>")
                # if work.chapters[i].chapterNum == -1:
                #     output.write(c)
                # else:
                if allinone and self.fileformat == 'pdf' and i == 0:
                    output.write('<a href="#tocbook-%s" ></a><p  id="tocbook-%s" class="civ_booktitle">%s</p>' % (
                        work.bookid, work.bookid, work.attrib('title')))
                output.write2( c, chapter)
                chapterpage = "%s-%02d" % (work.bookid, chapter)
                output.write(Footnote.GetPageEnd(chapterpage))
                if self.fileformat != 'pdf':
                    output.write(music.GetPageEnd(chapterpage, fileinfo))
                output.write("</section>")
                if self.fileformat != 'pdf':
                    output.write(HImage.GetPageEnd(chapterpage))

                output.close()


            self.attribs = output.attribs
            #self.output = output
        PageOutput.dump()
        return outputs
