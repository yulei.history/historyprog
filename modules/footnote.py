from modules.fileinfo import FileInfo
from modules.media import Media


class Footnote(Media):
    PageEnd = {}

    @staticmethod
    def reset():
        Footnote.PageEnd = {}

    def __init__(self, text='', indexes=None):
        super().__init__(indexes)
        self.MediaTitle = "脚注"
        self.media_type = 'footnote'
        self.media_note = ""
        self.media_label = "^"
        self.chapter = ''
        self.text = text
        self.linestr = ''

    def setData(self, linestr, chapter):
        self.text = linestr.strip()
        self.chapter = chapter
        self.linestr = linestr

    def outputToc(self, mediano, chapter):
        footinchapter = self.getMediaCount(chapter)
        note = "%s_%d" % (chapter, footinchapter)
        if self.fileformat == 'pdf':
            pageendhtml = ('<div class="civ_footnote" id="f%s"><a href="#footnote%s" class="civ_toca" >%d.  '
                           '</a>%s</div>') % (note, note, footinchapter, self.text)
        else:
            pageendhtml = '<div class="civ_footnote" id="f%s">%d.  <a href="#footnote%s">^</a>%s</div>' % (
                note, footinchapter, note, self.text)
        if not chapter in Footnote.PageEnd.keys():
            Footnote.PageEnd[chapter] = "<hr />\n"
        Footnote.PageEnd[chapter] += pageendhtml + "\n"
        foothtml = '<sup id="footnote%s"><a href="#f%s"  class="civ_toca" title="%d. %s">%d</a></sup>' % (
            note, note, footinchapter, self.text, footinchapter)
        return foothtml

    @staticmethod
    def count(media_type='footnote'):
        return Media.count('footnote')

    @staticmethod
    def GetPageEnd(chapter):
        if chapter in Footnote.PageEnd.keys():
            return Footnote.PageEnd[chapter]
        else:
            return ""


def ProcessFootnote(datastr, chapter, fileformat, file: FileInfo):
    footnote = Footnote()
    return footnote.ProcessToc(datastr, chapter, fileformat, file)
