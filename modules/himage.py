import os
import sys
import tempfile

from wand.image import Image

from modules.config import logger
from modules.fileinfo import FileInfo
from modules.media import Media, MediaUrl


class HImage(Media):
    # 静态数据
    bookname = ""
    __ImageDir = ""
    __OutputDir = ""
    __CacheDir = ""
    PageEnd = {}

    PAGE_MAXWIDTH = 450  # 页面显示最大宽度的设置
    SET_RATIO = 2  # 实际生成的图片是显示图片大小的倍率，这是为了使图片显示得更清晰

    @staticmethod
    def reset():
        HImage.__ImageDir = ""
        HImage.__OutputDir = ""
        HImage.__CacheDir = ""
        HImage.PageEnd = {}

    def getConstData(self):
        self.ifmap = self.media_type == 'map'
        if self.fileformat == 'pdf':
            if self.ifmap:
                self.PAGE_MAXWIDTH = 450
            else:
                self.PAGE_MAXWIDTH = 450
        else:
            if self.ifmap:
                self.PAGE_MAXWIDTH = 800
            else:
                self.PAGE_MAXWIDTH = 600

        if self.ifmap:
            self.SET_RATIO = 4
            self.MediaTitle = "地图"
        else:
            self.SET_RATIO = 2
        self.media_note = self.MediaTitle

    def __init__(self, image_type='image'):
        super().__init__()
        self.media_type = image_type
        self.MediaTitle = "插图"
        self.media_note = self.MediaTitle
        if self.media_type == 'map':
            self.ifmap = True
            self.media_label = "&"
        else:
            self.ifmap = False
            self.media_label = "!"
        self.ImageDir = HImage.__ImageDir + "/" + image_type + 's'
        self.OutputDir = HImage.__OutputDir
        self.getConstData()
        self.id = ''
        self.title = ''
        self.subtitle = ''
        self.other_str = ''
        self.image_src = ''
        self.other = ''
        self.linestr = ''

        logger.debug("输出目录如下%s" % self.OutputDir)

    def setData(self, linestr, chapter):
        self.linestr = linestr
        data = linestr.split('|')
        l = len(data)
        self.id = data[0]
        if l > 1:
            self.title = data[1].strip()
        if l > 2:
            self.subtitle = data[2].strip()
        if l > 3:
            self.urls = [MediaUrl(data[3].strip())]
        if l > 4:
            self.urls[0].url = data[4].strip()
        if l > 5 and self.fileformat == 'pdf':
            self.other_str = data[5].strip().lower()
        if l > 6 and self.fileformat == 'html':
            self.other_str = data[6].strip().lower()

        [self.image_src, self.other] = self.GetImageOther()

        # self.file = 'c%02d.html' % chapter

    # 处理图片的扩展选项
    def GetImageOther(self, tag='image'):
        imagenum = self.id

        # 图片的前缀有以下几种：
        #    原始图片： 以source_开头的变量
        #    生成图片： 以cache_开头的变量
        #    需要生成的图片： 以set_开头的变量(只有尺寸，没有实际的文件)
        #    显示于页面上： 以page_开头的变量(只有尺寸，没有实际的文件)

        # 判断源文件是否存在，生成图片的文件名
        exts = ('png', 'PNG', 'jpg', 'JPG', 'gif', 'GIF', 'jpeg', 'JPEG', 'svg', 'SVG')
        dest_file_name = "nothisfile"
        source_file = ""
        for ext in exts:
            source_file = self.ImageDir + "/" + imagenum + '.' + ext
            #logger.debug(source_file)
            if os.path.isfile(source_file):
                imgfile = os.path.basename(self.ImageDir) + "/" + imagenum + '.' + ext
                if ext == 'svg' or ext == 'SVG':
                    dest_file_name = os.path.basename(self.ImageDir) + "/" + HImage.bookname + "/" + imagenum + '.png'
                else:
                    dest_file_name = os.path.basename(self.ImageDir) + "/" + HImage.bookname+"/" + imagenum + '.' + ext
                break
        if dest_file_name == "nothisfile":
            logger.error(self.file.msg("没有这个文件：%s (%s)" % (imagenum, self.ImageDir + "/" + imagenum + '.')))
            return [dest_file_name, ""]

        # 缓存文件所在目录
        cache_file = self.__CacheDir + "/" + dest_file_name
        dest_file = self.OutputDir + "/" + dest_file_name
        if not os.path.exists(os.path.dirname(cache_file)):
            os.makedirs(os.path.dirname(cache_file))
        logger.debug("缓存图片文件：%s" % cache_file)

        # 取得源文件的各项信息
        source_size_bool = False
        source_size_filename = cache_file + '.source.size'
        source_width = 0
        source_height = 0
        if os.path.isfile(source_size_filename):
            source_size_time = os.path.getmtime(source_size_filename)
            source_time = os.path.getmtime(source_file)
            if source_time < source_size_time:
                source_size_file = open(source_size_filename, 'rt')
                source_size_str = source_size_file.read()
                source_size_file.close()
                try:
                    source_size_width_str, source_size_height_str = source_size_str.split(',')
                    source_width = int(source_size_width_str)
                    source_height = int(source_size_height_str)
                    source_size_bool = True
                except:
                    None

        source_image = None
        if not source_size_bool: # 如果没有.source.size文件或者源图片更加新，则读取源图片的尺寸数据
            if self.ifmap:
                # 因为inkscape读取svg文件时可能会输出一个警告，所以这里重定向错误输出以取得这信息
                fmessage = tempfile.TemporaryFile()
                fno = fmessage.fileno()
                ferr = sys.stderr.fileno()
                ferr_save = os.dup(ferr)
                os.dup2(fno, ferr)
                source_image = Image(filename=source_file, resolution=384)
                fmessage.seek(0)
                message = fmessage.read().decode('utf_8')
                fmessage.close()
                os.dup2(ferr_save, ferr)
                os.close(ferr_save)
                for line in message.splitlines():
                    line1 = line.strip()
                    if line1 != "":
                        logger.debug(line1)
            else:
                source_image = Image(filename=source_file)
            if source_image is None:
                logger.error(self.file.msg("读取文件错误：%s (%s)" % (imagenum, self.ImageDir + "/" + imagenum + '.')))
                return ['', '']
            source_height = source_image.height
            source_width = source_image.width
            # 将源图片的尺寸数据写入.source.size缓存文件
            source_size_file = open(source_size_filename, 'wt')
            source_size_file.write("%d,%d" % (source_width, source_height))
            source_size_file.close()
        # 以上为取得源文件的各项信息 取到的数据有：
        # source_image  源文件图片（有可能是None，如果从源文件source.size取到源文件大小的话
        # source_width, source_height 源图片尺寸

        # 读取工作文件中设置的图片选项，并计算出页面上显示的图片宽度
        if self.other_str.startswith("r"):
            rotate = True
            page_width_str = self.other_str[1:]
        else:
            rotate = False
            page_width_str = self.other_str

        try:
            page_width = int(page_width_str)
        except:
            # 工作文件中没有设置图片大小
            page_width = source_width
        if page_width > self.PAGE_MAXWIDTH:
            page_width = self.PAGE_MAXWIDTH
        # 以上为取得页面上显示的文件宽度：
        # page_width  无论是否旋转都是宽度
        # rotate是取到的是否旋转的布尔值

        # 生成图片的尺寸由页面显示尺寸乘以一个倍率
        set_width = page_width * self.SET_RATIO
        if rotate:
            set_height = int(set_width * source_width / source_height)
        else:
            set_height = int(set_width * source_height / source_width)
        # 是当需要生成的尺寸大于源文件尺寸时，直接采用源文件的尺寸
        if rotate:
            if set_width > source_height:
                set_width = source_height
                set_height = source_width
        else:
            if set_width > source_width:
                set_width = source_width
                set_height = source_height
        # 得到实际生成图片的尺寸，注意，如果旋转的，则是旋转后的
        #  set_width set_height

        # logger.debug('process : %s sourcew:%d page_w%d set_w%d ' %( imagenum, source_width, page_width, set_width))

        # 为提高速度，可能不需要重新生成图片，所以这里读入既有图片文件
        # 判断是否要重新生成文件看以下条件：
        #   缓存的文件比源文件新，并且图片尺寸与需求的一致
        cache_size_filename = cache_file + '.size'
        source_time = os.path.getmtime(source_file)
        if os.path.isfile(dest_file):
            cache_time = os.path.getmtime(dest_file)
            if os.path.isfile(cache_size_filename):
                cache_size_time = os.path.getmtime(cache_size_filename)
                if cache_size_time > cache_time > source_time:
                    cache_size_file = open(cache_size_filename, 'rt')
                    cache_size_width_str = cache_size_file.read()
                    cache_size_file.close()
                    try:
                        cache_size_width = int(cache_size_width_str)
                        if cache_size_width == set_width:
                            logger.debug(self.file.msg('process %s: %s cached(size cache)' % (tag, imagenum)))
                            return [dest_file_name, ' width=%d' % page_width]
                    except:
                        None

            if source_time < cache_time:
                # 插图图的情况下，如果缓存文件时间较新，且大小和需求的一致，则认为缓存可用，直接返回
                cache_image = Image(filename=dest_file)
                if cache_image is not None:
                    cache_width = cache_image.width
                    if cache_width == set_width:
                        cache_size_file = open(cache_size_filename, 'wt')
                        cache_size_file.write("%d" % cache_width)
                        cache_size_file.close()
                        logger.debug(self.file.msg('process %s: %s cached' % (tag, imagenum)))
                        return [dest_file_name, ' width=%d' % page_width]

        # 需要根据原始生成图片
        logger.debug(self.file.msg('process %s: %s' % (tag, imagenum)))

        if source_image is None:
            if self.ifmap:
                source_image = Image(filename=source_file, resolution=384)
            else:
                source_image = Image(filename=source_file)
            if source_image is None:
                logger.error(self.file.msg("读取文件错误：%s (%s)" % (imagenum, self.ImageDir + "/" + imagenum + '.')))
                return ['', '']

        set_image = source_image.clone()
        source_image.close()
        if rotate:
            set_image.rotate(270)

        if self.ifmap:
            set_image.resize(width=set_width, height=set_height, blur=0.1)
        else:
            set_image.resize(width=set_width, height=set_height)

        set_image.save(filename=dest_file)
        set_image.close()

        return [dest_file_name, ' width=%d' % page_width]

    def outputToc(self, mediano, chapter):
        tag = self.media_type
        source = self.urls[0].title
        url = self.urls[0].url
        title1 = '%s %d %s' % (self.MediaTitle, mediano, self.title)
        if self.fileformat == 'pdf':
            onclick = ''
        else:
            onclick = ('onclick="document.getElementById(\'pop_%s%d\').style.display '
                       '=\'block\';document.getElementById(\'fade\').style.display=\'block\'"') % (
                          tag, mediano)
        imagehtml = '<img class="civ_imagefile" src="%s" alt="%s" %s %s />' % (
            self.image_src, title1, self.other, onclick)
        #        if self.fileformat == 'pdf':
        pdfhtml = '<h9 class="civ_hidden" tag="%s" id="a_%s%d" subtitle="%s" source="%s" sourceurl="%s" >%s</h9>' \
                  % (tag, tag, mediano, self.subtitle, source, url, title1)
        # h9这个隐藏标签目的是生成 toc文件，第二次生成PDF时，读取toc文件生成页码
        #        else:
        #            pdfhtml = ''
        rtn = '%s<div class="civ_image" >%s<div class="civ_image_title">%s %s</div></div>' \
              % (pdfhtml, imagehtml, title1, self.subtitle)

        pageendhtml = ('<img id="pop_%s%d" class="popup_img" src="%s" alt="%s" onclick = "document.getElementById('
                       '\'pop_%s%d\').style.display =\'none\' ;document.getElementById('
                       '\'fade\').style.display=\'none\'">') % (
                          tag, mediano, self.image_src, title1, tag, mediano)
        if chapter in HImage.PageEnd.keys():
            HImage.PageEnd[chapter] += pageendhtml + "\n"
        else:
            HImage.PageEnd[chapter] = pageendhtml + "\n"
        return rtn

    @staticmethod
    def SetDirs(inputdirname, outputdirname, cachedirname, bookname = ""):
        # HImages.__mapdir = inputdirname + "/maps"
        # HImages.__imagedir = inputdirname + "/images"
        if not os.path.isdir(inputdirname):
            logger.error('指定目录下没有目录:%s' % inputdirname)
            return False
        HImage.__ImageDir = inputdirname
        HImage.__OutputDir = outputdirname
        HImage.__CacheDir = cachedirname
        HImage.bookname = bookname
        logger.debug('指定输出目录:%s' % HImage.__OutputDir)
        if not (os.path.exists(outputdirname)):
            os.makedirs(name=outputdirname, exist_ok=True)

        if not (os.path.exists(outputdirname + '/images')):
            os.makedirs(name=outputdirname + '/images', exist_ok=True)

        if not (os.path.exists(outputdirname + '/maps')):
            os.makedirs(name=outputdirname + '/maps', exist_ok=True)

        if len(bookname) > 0 :
            if not (os.path.exists(outputdirname + '/images/' + bookname)):
                os.makedirs(name=outputdirname + '/images/'+ bookname, exist_ok=True)

            if not (os.path.exists(outputdirname + '/maps/'+ bookname)):
                os.makedirs(name=outputdirname + '/maps/'+ bookname, exist_ok=True)
        return True

    @staticmethod
    def GetPageEnd(chapter):
        if chapter in HImage.PageEnd.keys():
            return HImage.PageEnd[chapter]
        else:
            return ""

    @staticmethod
    def ProcessImage(datastr, chapter, fileformat, file: FileInfo):
        image = HImage('image')
        return image.ProcessToc(datastr, chapter, fileformat, file)

    @staticmethod
    def ProcessMap(datastr, chapter, fileformat, file: FileInfo):
        image = HImage('map')
        return image.ProcessToc(datastr, chapter, fileformat, file)
