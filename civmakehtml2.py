#!/usr/bin/env python
# -*- coding: utf-8 -*-

import getopt
import logging
import os
import shutil
import subprocess
import sys
import tempfile

import modules.mathsvg
from modules.config import logger
from modules.makehtml import MakeHtml
from modules.output import Output


# 只能在Linux下运行，（我在ManjaroLinux上工作，其他的发行版没有试过）
# 需要安装：
#     python的 wand 和 pypinyin, pypdf 模块
#     openssl-1.1 库
#     node.js 以及node.js的 mathjax-node-sre 库 （用于处理嵌入的数学公式）
#     字体： "AR PL UKai CN",  "AR PL UMing CN"（ttf-arphic-uming ttf-arphic-ukai）对应Windows下的 "KaiTi", "SimSun",
#     inkscape（外部程序，wand库用以转换SVG文件）

def Usage():
    print(
        "Usage: pythone3 civmakehtml2.py [--input_dir=工作文件目录] [--output_dir=输出目录] 部分编号 [工作文件号]"
    )
    exit()


def RunWk(wkcmdline):
    logger.debug(wkcmdline)
    wkprocess = subprocess.Popen(wkcmdline, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True)
    while True:
        line = wkprocess.stdout.readline()
        if line == '' and wkprocess.poll() is not None:
            break
        if line:
            if line[0:1] != "[":
                if line[0:6].lower() == 'debug:':
                    logger.debug("wkhtmltopdf:" + line[6:].strip())
                else:
                    logger.info("wkhtmltopdf:" + line.strip())

    returnc = wkprocess.poll()
    return returnc


def ClearOutputDir(outputdir):
    if os.path.exists(outputdir):
        os.system("rm -rf %s/*.html" % outputdir)
        os.system("rm -rf %s/*.css" % outputdir)
    else:
        os.makedirs(outputdir)
    os.system("cp -rf ./others/* %s" % outputdir)


def ProcessBookHTML(book, booklist):
    ClearOutputDir(os.path.join(OUTPUT, book, 'html'))

    globalcachedir = os.path.join(OUTPUT, 'cache', testenv)
    os.makedirs(globalcachedir, exist_ok=True)

    inputdirname = os.path.join(WORKFILEPATH)

    # 制作HTML文件输出
    outputdirname = os.path.join(OUTPUT, book, 'html')
    localcachedir = os.path.join(globalcachedir, book, 'html')
    os.makedirs(localcachedir, exist_ok=True)
    makehtml = MakeHtml(inputdirname, workfile, outputdirname, globalcachedir, localcachedir, '',
                        'html')
    if book == 'all':
        makehtml.runAll(booklist)
    else:
        makehtml.runAll([book])

    if makehtml.error_exit:
        logger.info("html处理结束(有错误) %s" % book)
        quit()


def ProcessBookPdfAllinOne( booklist):
    ClearOutputDir(os.path.join(OUTPUT, 'all', 'htmlpdf'))

    globalcachedir = os.path.join(OUTPUT, 'cache', testenv)
    os.makedirs(globalcachedir, exist_ok=True)

    inputdirname = os.path.join(WORKFILEPATH)

    # 制作PDF文件输出
    outputdirname = os.path.join(OUTPUT, 'all', 'htmlpdf')
    localcachedir = os.path.join(globalcachedir, 'all', 'htmlpdf')
    os.makedirs(localcachedir, exist_ok=True)
    pdfinpitdir = outputdirname
    makehtml = MakeHtml(inputdirname, '', outputdirname, globalcachedir, localcachedir, '',
                        'pdf')
    makehtml.runAll(booklist)

    if makehtml.error_exit:
        logger.info("第一遍pdf用html处理结束(有错误) %s" % 'all')
        quit()

    toc_xml_file = tempfile.NamedTemporaryFile(suffix=".xml")
    temp_pdf = tempfile.NamedTemporaryFile(suffix=".pdf")

    outputpdffile = os.path.join(OUTPUT, 'all' + ".pdf")

    VERSION = makehtml.attribs['version']

    wkcmd = WK + """ \
       --log-level %s \
       --dump-outline "%s"  --no-outline --enable-local-file-access \
       -s A5 -B 20 -T 20 -L 20 -R 20 --header-font-size 10 --header-spacing 4 \
       --header-line  --header-left "[title]" --header-right '[section2]' \
       --footer-left "%s" --footer-right '[page]/[topage]' --footer-spacing 3 \
       --footer-line --footer-font-size 10 cover "%s"/index.html \
       cover "%s"/index1.html \
       "%s"/toc.html \
       "%s"/c*.html  \
       "%s"
       """ % (
        wkloglevel, toc_xml_file.name, VERSION, pdfinpitdir, pdfinpitdir, pdfinpitdir, pdfinpitdir, temp_pdf.name)

    RunWk(wkcmd)

    makehtml = MakeHtml(inputdirname, workfile, outputdirname, globalcachedir, localcachedir, toc_xml_file.name,
                        'pdf')
    outputs = makehtml.runAll(booklist)
    if makehtml.error_exit:
        logger.info("第二遍pdf用html处理结束(有错误) %s" % 'all')
        quit()

    RunWk(wkcmd)
    Output.PdfOutline(temp_pdf.name, outputpdffile, outputs)


def ProcessBookPdf(book, split='', splitabc='', startchapter=0, attrib=None):
    ClearOutputDir(os.path.join(OUTPUT, book + splitabc, 'htmlpdf'))

    globalcachedir = os.path.join(OUTPUT, 'cache', testenv)
    os.makedirs(globalcachedir, exist_ok=True)

    inputdirname = os.path.join(WORKFILEPATH)

    outputdirname = os.path.join(OUTPUT, book + splitabc, 'htmlpdf')
    localcachedir = os.path.join(globalcachedir, book + splitabc, 'htmlpdf')
    os.makedirs(localcachedir, exist_ok=True)
    pdfinpitdir = outputdirname
    makehtml = MakeHtml(inputdirname, workfile, outputdirname, globalcachedir, localcachedir, '',
                        'pdf')

    if attrib is not None:
        makehtml.attribs = attrib
    makehtml.runAll([book], split, splitabc, startchapter)

    if makehtml.error_exit:
        logger.info("第一遍pdf用html处理结束(有错误) %s" % book)
        quit()

    toc_xml_file = tempfile.NamedTemporaryFile(suffix=".xml")
    temp_pdf = tempfile.NamedTemporaryFile(suffix=".pdf")

    if workfile == '':
        outputpdffile = os.path.join(OUTPUT, book + splitabc + ".pdf")
    else:
        outputpdffile = os.path.join(OUTPUT, book + splitabc + "-" + workfile + ".pdf")

    TITLE = makehtml.attribs['title']
    VERSION = makehtml.attribs['version']

    wkcmd = WK + """ \
        --log-level %s \
        --dump-outline "%s" --outline --enable-local-file-access \
        -s A5 -B 20 -T 20 -L 20 -R 20 --header-font-size 10 --header-spacing 4 \
        --header-line  --header-left "%s" --header-right '[section2]' \
        --footer-left "%s" --footer-right '[page]/[topage]' --footer-spacing 3 \
        --footer-line --footer-font-size 10 cover "%s"/index.html \
        cover "%s"/index1.html \
        toc --disable-dotted-lines --xsl-style-sheet pdf/civtoc.xslt \
        "%s"/c*.html  \
        "%s"
        """ % (
        wkloglevel, toc_xml_file.name, TITLE, VERSION, pdfinpitdir, pdfinpitdir, pdfinpitdir, temp_pdf.name)

    RunWk(wkcmd)

    makehtml = MakeHtml(inputdirname, workfile, outputdirname, globalcachedir, localcachedir, toc_xml_file.name,
                        'pdf')
    if attrib is not None:
        makehtml.attribs = attrib
    makehtml.runAll([book], split, splitabc, startchapter)
    if makehtml.error_exit:
        logger.info("第二遍pdf用html处理结束(有错误) %s" % book)
        quit()

    RunWk(wkcmd)
    shutil.copy2( temp_pdf.name, outputpdffile)
    return makehtml.lastchapter, makehtml.attribs


def ProcessBook(book, booklist, split=''):
    logger.info("处理开始 (%s)" % book)

    ProcessBookHTML(book, booklist)

    if book == 'all':
        ProcessBookPdfAllinOne(booklist)
    else:

        if split != '':
            lastchapter, attrib = ProcessBookPdf(book, split, '', 0)
            ProcessBookPdf(book, split, 'b', lastchapter, attrib)
        else:
            ProcessBookPdf(book, split, '', 0)

    logger.info("处理结束 %s" % book)


# 主程序这里开始
# 输入的WORK文件所在目录
WORKFILEPATH = './civ/'
# 输出的目录
OUTPUT = './output'

try:
    options, args = getopt.getopt(sys.argv[1:], "tdD",
                                  ["input_dir=", "output_dir=", "log=", "one", "split=", "test"])
except getopt.GetoptError:
    Usage()

verbosedebug = False
testenv = ""
logname = os.getenv("history_log")
wkloglevel = "info"
loglevel = logging.INFO
allinone = False  # 各部分生成在一个内，否则各部分分别生成
splittext = ''

for o, a in options:
    if o in ("--output_dir"):
        OUTPUT = a
    elif o in ("--input_dir"):
        WORKFILEPATH = a
    elif o == "-d":
        testenv = "/debug"
        loglevel = logging.DEBUG
    elif o == "-D":
        testenv = "/debug"
        wkloglevel = "debug"
        loglevel = logging.DEBUG
    elif o in ("-t", "--test"):
        testenv = "/test"
    elif o in ("--one"):
        allinone = True
    elif o == "--split":
        splittext = a
    elif o == "--log":
        logname = a

if not os.path.exists(OUTPUT):
    os.makedirs(OUTPUT)

if logname == "" or logname is None:
    logFile = logging.FileHandler(os.path.join(OUTPUT, 'output.log'))
elif len(logname) > 2 and logname.lower()[:2] == "no":
    logFile = None
else:
    logFile = logging.FileHandler(logname)

if not logFile is None:
    logFile.setFormatter(logging.Formatter(modules.config.LOGGING['formatters']['verbose']['format']))
    logFile.setLevel(logging.DEBUG)
    logger.addHandler(logFile)

logOutput = logging.StreamHandler(sys.stderr)
logOutput.setFormatter(logging.Formatter(modules.config.LOGGING['formatters']['normal']['format']))
logOutput.setLevel(loglevel)
logger.addHandler(logOutput)

if allinone and splittext != '':
    logger.error("不能同时指定--one和--split选项")
    quit()

proxy = os.getenv("https_proxy")
if proxy is not None:
    WK = "export LD_LIBRARY_PATH=./wk; ./wk/wkhtmltopdf -p %s " % proxy
else:
    WK = "export LD_LIBRARY_PATH=./wk; ./wk/wkhtmltopdf "

booklist = []

files = os.listdir(WORKFILEPATH)
for file in files:
    file_path = os.path.join(WORKFILEPATH, file)
    if file[0] != '.' and file[0] != '_' and os.path.isdir(file_path):
        booklist.append(file)

booklist.sort()
workfile = ''
if len(args) > 0:
    book = args[0]
    if not book in booklist:
        dir_path = os.path.join(WORKFILEPATH, book)
        if not os.path.isdir(dir_path):
            logger.error("没有%s，全部的列表是%s" % (book, ",".join(booklist)))
            quit()
    booklist = [book]

    if len(args) > 1:
        workfile = args[1] + ".work"
        workfile_path = os.path.join(WORKFILEPATH, book, workfile)
        if not os.path.exists(workfile_path):
            logger.error("文件%s不存在 " % workfile_path)
            quit()

if len(booklist) == 1:
    thebook = booklist[0]
else:
    thebook = None

splitinfo = {}
if splittext != '':
    for splitpart in splittext.split(","):
        p = splitpart.split(":")
        if len(p) == 1:
            if thebook is None:
                logger.error("split中不能指定了章节却没有指定是哪部分")
                quit()
            else:
                splitinfo[thebook] = p[0]
        else:
            if p[0] in booklist:
                splitinfo[p[0]] = p[1]
            else:
                logger.error("%s 并不在处理的列表中 " % p[0])
                quit()

if allinone:
    ProcessBook('all', booklist)
else:
    for book in booklist:
        if book in splitinfo.keys():
            ProcessBook(book, booklist, splitinfo[book])
        else:
            ProcessBook(book, booklist)
